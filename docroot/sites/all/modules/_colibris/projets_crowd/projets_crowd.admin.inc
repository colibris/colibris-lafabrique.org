<?php
function projets_crowd_admin_settings($form, &$form_state) {
	$form['projets_crowd'] = array(
		'#title' => t('Configuration API plate-formes crowdfunding'),
		'#type'  => 'fieldset',
		'#collapsible' => FALSE,
	);
		$form['projets_crowd']['lanef'] = array(
			'#title' => t('La Nef'),
			'#type'  => 'fieldset',
			'#collapsible' => FALSE,
		);
			$form['projets_crowd']['lanef']['projets_crowd_lanef_url'] = array(
				'#title'  => t('Domaine'),
				'#description'  => t('le domaine est utilisé pour la reconnaissance des URL dans les besoins de financement'),
				'#type'  => 'textfield',
				'#default_value' => variable_get('projets_crowd_lanef_url',FALSE),
				'#rows' => 20,
			);
			$form['projets_crowd']['lanef']['projets_crowd_lanef_endpoint'] = array(
				'#title'  => t('XML'),
				'#description'  => t('URL du fichier XML'),
				'#type'  => 'textfield',
				'#default_value' => variable_get('projets_crowd_lanef_endpoint',FALSE),
				'#rows' => 20,
			);
	return system_settings_form($form);
}
