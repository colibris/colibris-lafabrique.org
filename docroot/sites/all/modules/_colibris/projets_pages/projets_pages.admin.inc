<?php
function projets_pages_admin_settings($form, &$form_state) {
	$form['projets_pages'] = array(
		'#title' => t('Contenu des pages statiques'),
		'#type'  => 'fieldset',
		'#collapsible' => FALSE,
	);
	$form['projets_pages']['projets_pages_fabrique'] = array(
		'#title'  => t('Page "La Fabrique"'),
		'#type'  => 'textarea',
		'#rows' => 20,
		'#default_value' => variable_get('projets_pages_fabrique',FALSE),
	);
	$form['projets_pages']['projets_pages_guide'] = array(
		'#title'  => t('Page "Comment ça marche ?"'),
		'#type'  => 'textarea',
		'#default_value' => variable_get('projets_pages_guide',FALSE),
		'#rows' => 20,
	);
	$form['projets_pages']['projets_pages_crowd'] = array(
		'#title'  => t('Page crowdfunding'),
		'#type'  => 'textarea',
		'#rows' => 20,
		'#default_value' => variable_get('projets_pages_crowd',FALSE),
	);
	$form['projets_pages']['projets_pages_soutenir'] = array(
		'#title'  => t('Page "Soutenir Colibris"'),
		'#type'  => 'textarea',
		'#rows' => 20,
		'#default_value' => variable_get('projets_pages_soutenir',FALSE),
	);
	return system_settings_form($form);
}
