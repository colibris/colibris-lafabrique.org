<?php

/**
 * @file
 * hebergements.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hebergements_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function hebergements_node_info() {
  $items = array(
    'offres_hebergement' => array(
      'name' => t('Offres d\'hébergement'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
