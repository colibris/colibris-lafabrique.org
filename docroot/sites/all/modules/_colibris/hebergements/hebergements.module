<?php

/**
 * @file
 * Code for the Hébergements feature.
 */

include_once 'hebergements.features.inc';

/**
 * get all hebergements nodes with hebergements url
 */
function _fetch_hebergements()
{
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'offres_hebergement')
        ->propertyCondition('status', NODE_PUBLISHED);
    $result = $query->execute();
    if (isset($result['node'])) {
        $nids = array_keys($result['node']);
    }

    return $nids;
}

/**
 * Implements hook_cron().
 */
function hebergements_cron()
{
    $now = time();
    $last_update = variable_get('hebergements_last_sync', false);
    //$period = 3 * 60 * 60;	// 3 hours
    $period = 0;	// always

    if ($now - $last_update > $period) {
        $hebergements_nids = _fetch_hebergements();
        // on efface tout l'existant en mode yolo
        if (!empty($hebergements_nids)) {
            node_delete_multiple($hebergements_nids);
            drupal_set_message(t('Suppression de %count offre(s) d\'hébergement.', array('%count' => count($hebergements_nids))));
        }
        // Create a stream
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept: application/ld+json\r\n"
            )
        );
        $context = stream_context_create($opts);
        $newHebergement = file_get_contents('https://colibris.social/lafabrique/hosting-services', false, $context);
        $tabHebergement = json_decode($newHebergement, true);
        //$queue = DrupalQueue::get('hebergements');
        drupal_set_message(t('Ajout de %count offre(s) d\'hébergement.', array('%count' => count($tabHebergement["ldp:contains"]))));
        foreach ($tabHebergement["ldp:contains"] as $heb) {
            //$queue->createItem($heb);
            hebergements_create_data($heb);
        }
        variable_set('hebergements_last_sync', $now);
    }
}

/**
 * Implementation of hook_cron_queue_info().
 */
function hebergements_cron_queue_info()
{
    $queues['hebergements'] = array(
      'worker callback' => 'hebergements_create_data',
    );
    return $queues;
}

/* */
function hebergements_batch($progressive = false)
{
    $hebergements_nids = _fetch_hebergements();
    $operations = array();

    foreach ($hebergements_nids as $heb) {
        $operations[] = array('hebergements_create_data', array($heb, true, true));
    }

    $batch = array(
      'operations' => $operations,
      'progressive' => $progressive,
      'progress_message' => t('Processing  @current of @total tasks (ETA: @estimate)')
    );

    batch_set($batch);
    batch_process('');
}

/**
 * Update all hebergements nodes or just one
 */

function hebergements_create_data($infos, $save = true, $debug = false)
{
    global $user;

    $node = new stdClass();
    $node->type = 'offres_hebergement';
    node_object_prepare($node);

    $node->uid = $user->uid;

    $node->language = LANGUAGE_NONE;
    //$node->field_node_refrence_field[$node->language][0]['nid'] = $nid-of-reference-field;
    $node->title = $infos['pair:label'] ?? '';
    if (!empty($infos['pair:description'])) {
        $node->body['fr'][0]['value'] = $infos['pair:description'] ?? '';
    }
    if (!empty($infos['oasis:bookingUrl'])) {
        $node->field_bookingurl['und'][0]['url'] = $infos['oasis:bookingUrl'] ?? '';
        $node->field_bookingurl['und'][0]['original_url'] = $infos['oasis:bookingUrl'] ?? '';
    }
    if (!empty($infos['oasis:image'])) {
        if (is_array($infos['oasis:image'])) {
            foreach ($infos['oasis:image'] as $key => $val) {
                $node->field_imageurl['und'][$key]['value'] = $val ?? '';
            }
        } else {
            $node->field_imageurl['und'][0]['value'] = $infos['oasis:image'] ?? '';
        }
    }
    if (!empty($infos['oasis:maxCapacity'])) {
        $node->field_maxcapacity['und'][0]['value'] = $infos['oasis:maxCapacity'] ?? '';
    }
    if (!empty($infos['oasis:minCapacity'])) {
        $node->field_mincapacity['und'][0]['value'] = $infos['oasis:minCapacity'] ?? '';
    }
    if (!empty($infos['oasis:price'])) {
        $node->field_price['und'][0]['value'] = $infos['oasis:price'] ?? '';
    }
    if (!empty($infos['pair:offeredBy'])) {
        // Create a stream
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept: application/ld+json\r\n"
            )
        );
        $context = stream_context_create($opts);
        $proj = file_get_contents($infos['pair:offeredBy'], false, $context);
        $proj = json_decode($proj, true);
        if (!empty($proj['pair:aboutPage'])) {
            $p = explode('projets/', $proj['pair:aboutPage']);
            $normal_path = drupal_get_normal_path('les-projets/'.$p[1]);
            $nid = str_replace("node/", '', $normal_path);
            $node->field_projet['und'][0]['target_id'] = $nid;
        }
    }
    $node = node_submit($node);
    node_save($node);
}
