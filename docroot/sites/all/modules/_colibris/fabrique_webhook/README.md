## Fabrique webhook module

The webhook module of La Fabrique allows you to configure the sending of POST requests when entities are added/modified/deleted.

### Installation

With the administration tool `drush`, you can install the module by typing:

- ```drush en fabrique_webhook```
- ```drush cc all``` (clear all caches)

### Configuration

The module has an web interface to configure it. Please find it the administration space: 'Configuration' tab, 'System' section.
It's called 'Webhook La Fabrique'.

### Logging

For each post request send by the webhook module, a log message is generated. It shows the status of the request (200 OK or any
response error code) and all the details.

You can see the log messages with the command `drush ws`. To see the last 100 log message, you can type:
```drush ws --count=100```  (with 100 the number of the last comments showed)\
Or if you want to see the logs in realtime, execute:
```drush ws --tail```


