<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fabrique_specific_blocks_and_menus_ctools_plugin_api($module = null, $api = null)
{
    if ($module == "context" && $api == "context") {
        return array("version" => "3");
    }
    if ($module == "field_group" && $api == "field_group") {
        return array("version" => "1");
    }
    if ($module == "property_validation" && $api == "default_property_validation_rules") {
        return array("version" => "2");
    }
    if ($module == "strongarm" && $api == "strongarm") {
        return array("version" => "1");
    }
}

/**
 * Implements hook_views_api().
 */
function fabrique_specific_blocks_and_menus_views_api($module = null, $api = null)
{
    return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function fabrique_specific_blocks_and_menus_image_default_styles()
{
    $styles = array();

    // Exported image style: diaporama.
    $styles['diaporama'] = array(
    'label' => 'diaporama',
    'effects' => array(
      6 => array(
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => 0,
          'ypos' => 0,
          'alpha' => 0,
          'path' => 'insetTravers.png',
        ),
        'weight' => 2,
      ),
    ),
  );

    // Exported image style: fb.
    $styles['fb'] = array(
    'label' => 'fb',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
        ),
        'weight' => 1,
      ),
    ),
  );

    // Exported image style: projet_large.
    $styles['projet_large'] = array(
    'label' => 'projet large',
    'effects' => array(
      21 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 730,
          'height' => 420,
        ),
        'weight' => 1,
      ),
    ),
  );

    // Exported image style: projet_map.
    $styles['projet_map'] = array(
    'label' => 'projet map',
    'effects' => array(
      20 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 150,
          'height' => 150,
        ),
        'weight' => 1,
      ),
    ),
  );

    // Exported image style: projet_thumb.
    $styles['projet_thumb'] = array(
    'label' => 'projet thumb',
    'effects' => array(
      23 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 60,
          'height' => 45,
        ),
        'weight' => 1,
      ),
    ),
  );

    // Exported image style: projets.
    $styles['projets'] = array(
    'label' => 'projets',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 215,
        ),
        'weight' => -9,
      ),
    ),
  );

    // Exported image style: slider.
    $styles['slider'] = array(
    'label' => 'Slider',
    'effects' => array(
      24 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 950,
          'height' => 370,
        ),
        'weight' => 1,
      ),
    ),
  );

    // Exported image style: user.
    $styles['user'] = array(
    'label' => 'user',
    'effects' => array(
      22 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 130,
          'height' => 130,
        ),
        'weight' => 1,
      ),
    ),
  );

    return $styles;
}

/**
 * Implements hook_node_info().
 */
function fabrique_specific_blocks_and_menus_node_info()
{
    $items = array(
    'actualit_s' => array(
      'name' => t('Actualités'),
      'base' => 'node_content',
      'description' => t('Brèves pour la page d\'accueil'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'need_money' => array(
      'name' => t('Campagne crowfunding'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'need_people' => array(
      'name' => t('Besoin compétence'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre : "Je recherche..."'),
      'help' => '',
    ),
    'need_stuff' => array(
      'name' => t('Besoin matériel'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre : "Je recherche..."'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page orpheline'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>pages orpheline</em> pour votre contenu statique, tel que la page \'Qui sommes-nous\'.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'projet' => array(
      'name' => t('Projet'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nom du projet'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
    drupal_alter('node_info', $items);
    return $items;
}
