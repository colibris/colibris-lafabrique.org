<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function fabrique_specific_blocks_and_menus_default_rules_configuration() {
  $items = array();
  $items['rules_contact_projet'] = entity_import('rules_config', '{ "rules_contact_projet" : {
      "LABEL" : "Contact projet",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "webform_rules", "php" ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "webform_has_id" : {
            "form_id" : "[form-id:value]",
            "selected_webform" : { "value" : { "webform-client-form-1567" : "webform-client-form-1567" } }
          }
        },
        { "php_eval" : { "code" : "if ($data[\\u0027components\\u0027][\\u0027besoin\\u0027][\\u0027value\\u0027][0] == \\u0027\\u0027) {\\r\\n  return TRUE;\\r\\n} else {\\r\\n  return FALSE;\\r\\n}" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "\\u003C?php\\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$author = user_load($projet-\\u003Euid);\\r\\necho $author-\\u003Email;\\r\\n?\\u003E",
            "subject" : "\\u003C?php \\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$projet_name = $projet-\\u003Etitle;\\r\\n?\\u003E\\r\\nun message au sujet de votre projet \\u0022\\u003C?php echo $projet_name;?\\u003E\\u0022",
            "message" : "\\u003C?php \\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$projet_name = $projet-\\u003Etitle;\\r\\n$besoin = node_load($data[\\u0027components\\u0027][\\u0027besoin\\u0027][\\u0027value\\u0027][0]);\\r\\n$besoin_name = $besoin-\\u003Etitle;\\r\\n?\\u003E\\r\\nBonjour,\\u003Cbr\\/\\u003E\\r\\nVous avez re\\u00e7u un message via la Fabrique des colibris, au sujet de votre projet \\u0022\\u003C?php echo $projet_name;?\\u003E\\u0022.\\r\\nAttention, pour y r\\u00e9pondre, veillez \\u00e0 bien copier-coller l\\u0027adresse e-mail de l\\u0027exp\\u00e9diteur du message et \\u00e0 ne pas cliquer directement sur \\u0022R\\u00e9pondre\\u0022 (sinon, votre message sera envoy\\u00e9 aux administrateurs de la Fabrique des colibris).\\r\\n\\r\\n---------------------------------------------------------------\\r\\nPr\\u00e9nom: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027prenom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nNom: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027nom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nAdresse e-mail: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027email\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nMessage: \\r\\n\\u003C?php echo $data[\\u0027components\\u0027][\\u0027message\\u0027][\\u0027value\\u0027][0]; ?\\u003E",
            "from" : "\\u003C?php echo $data[\\u0027components\\u0027][\\u0027prenom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003C\\u003C?php echo $data[\\u0027components\\u0027][\\u0027email\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003E",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_logout'] = entity_import('rules_config', '{ "rules_logout" : {
      "LABEL" : "logout",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_logout" : [] },
      "DO" : [ { "redirect" : { "url" : "\\u003Cfront\\u003E" } } ]
    }
  }');
  $items['rules_new_projet'] = entity_import('rules_config', '{ "rules_new_projet" : {
      "LABEL" : "nouveau projet",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--projet" : { "bundle" : "projet" } },
      "DO" : [
        { "mail" : {
            "to" : "lafabrique@colibris-lemouvement.org",
            "subject" : "Nouveau projet sur la plate-forme Colibris",
            "message" : "Un nouveau projet vient d\\u0027\\u00eatre publi\\u00e9 sur la plate-forme :\\r\\n\\r\\n[node:url]\\r\\n",
            "from" : "noreply@colibris-lemouvement.org",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Acc\\u00e9l\\u00e9rez la mise en ligne de votre projet !",
            "message" : "Bonjour,\\r\\n\\r\\nF\\u00e9licitations, votre projet a bien \\u00e9t\\u00e9 enregistr\\u00e9 sur la Fabrique des colibris ! Nous traitons votre demande de mise en ligne au plus vite. \\r\\n\\r\\nAfin de r\\u00e9duire le d\\u00e9lai de mod\\u00e9ration de votre projet, nous vous conseillons de v\\u00e9rifier les points suivants et d\\u2019effectuer des modifications si besoin : \\r\\n- Vous \\u00eates-vous pr\\u00e9sent\\u00e9.e en quelques mots et avez-vous ins\\u00e9r\\u00e9 une photo de vous\\/de votre collectif ? (voir \\u201cModifier mon profil\\u201d) \\r\\n- Avez-vous d\\u00e9crit votre projet de fa\\u00e7on claire et concise (historique de la cr\\u00e9ation, mission, participants du projet, prochaines \\u00e9tapes,\\u2026) et ajout\\u00e9 des photos qui l\\u2019illustrent ? \\r\\n- Avez-vous inscrit vos besoins dans les rubriques pr\\u00e9vues \\u00e0 cet effet  \\u201cje cherche \\u2026 du financement\\/des ressources humaines\\/du mat\\u00e9riel\\u201d et les avez-vous prioris\\u00e9s (possibilit\\u00e9 de mettre en ligne 3 demandes par type simultan\\u00e9ment) ? \\r\\n\\r\\nPour parfaire la pr\\u00e9sentation de votre projet, pensez \\u00e0 vous appuyer sur le guide du porteur de projet pr\\u00e9vu \\u00e0 cet effet : https:\\/\\/www.colibris-lafabrique.org\\/sites\\/all\\/docs\\/Guideduporteurdeprojet_laFabrique_V1.pdf\\r\\n\\r\\nNous faisons notre possible pour vous assurer un accompagnement au plus pr\\u00e8s de vos besoins et restons \\u00e0 votre disposition si vous avez des questions : lafabrique@colibris-lemouvement.org\\r\\n\\r\\nA bient\\u00f4t !\\r\\n\\r\\nBien cordialement, \\r\\nL\\u2019\\u00e9quipe de Colibris ",
            "from" : "La Fabrique des colibris \\u003Clafabrique@colibris-lemouvement.org\\u003E",
            "language" : [ "site:current-user:language" ]
          }
        }
      ]
    }
  }');
  $items['rules_proj_ctc_besoin'] = entity_import('rules_config', '{ "rules_proj_ctc_besoin" : {
      "LABEL" : "Contact projet avec besoin",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "webform_rules", "php" ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "webform_has_id" : {
            "form_id" : "[form-id:value]",
            "selected_webform" : { "value" : { "webform-client-form-1567" : "webform-client-form-1567" } }
          }
        },
        { "php_eval" : { "code" : "if ($data[\\u0027components\\u0027][\\u0027besoin\\u0027][\\u0027value\\u0027][0] != \\u0027\\u0027) {\\r\\n  return TRUE;\\r\\n} else {\\r\\n  return FALSE;\\r\\n}" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "\\u003C?php\\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$author = user_load($projet-\\u003Euid);\\r\\necho $author-\\u003Email;\\r\\n?\\u003E",
            "subject" : "\\u003C?php \\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$projet_name = $projet-\\u003Etitle;\\r\\n$besoin = node_load($data[\\u0027components\\u0027][\\u0027besoin\\u0027][\\u0027value\\u0027][0]);\\r\\n$besoin_name = $besoin-\\u003Etitle;\\r\\n?\\u003E\\r\\nUne r\\u00e9ponse \\u00e0 votre demande \\u0022\\u003C?php echo $besoin_name;?\\u003E\\u0022 du projet \\u0022\\u003C?php echo $projet_name;?\\u003E\\u0022",
            "message" : "\\u003C?php \\r\\n$projet = node_load($data[\\u0027components\\u0027][\\u0027projet\\u0027][\\u0027value\\u0027][0]);\\r\\n$projet_name = $projet-\\u003Etitle;\\r\\n$besoin = node_load($data[\\u0027components\\u0027][\\u0027besoin\\u0027][\\u0027value\\u0027][0]);\\r\\n$besoin_name = $besoin-\\u003Etitle;\\r\\n?\\u003E\\r\\nBonjour,\\u003Cbr\\/\\u003E\\r\\nVous avez re\\u00e7u une r\\u00e9ponse \\u00e0 votre demande \\u0022\\u003C?php echo $besoin_name;?\\u003E\\u0022 du projet \\u0022\\u003C?php echo $projet_name;?\\u003E\\u0022, sur la Fabrique des colibris.\\r\\nAttention, pour y r\\u00e9pondre, veillez \\u00e0 bien copier-coller l\\u0027adresse e-mail de l\\u0027exp\\u00e9diteur du message et \\u00e0 ne pas cliquer directement sur \\u0022R\\u00e9pondre\\u0022 (sinon, votre message sera envoy\\u00e9 aux administrateurs de la Fabrique des colibris).\\r\\n\\r\\n---------------------------------------------------------------\\r\\nPr\\u00e9nom: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027prenom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nNom: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027nom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nAdresse e-mail: \\u003C?php echo $data[\\u0027components\\u0027][\\u0027email\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003Cbr\\/\\u003E\\r\\nMessage: \\r\\n\\u003C?php echo $data[\\u0027components\\u0027][\\u0027message\\u0027][\\u0027value\\u0027][0]; ?\\u003E",
            "from" : "\\u003C?php echo $data[\\u0027components\\u0027][\\u0027prenom\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003C\\u003C?php echo $data[\\u0027components\\u0027][\\u0027email\\u0027][\\u0027value\\u0027][0]; ?\\u003E\\u003E",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_refus_projet'] = entity_import('rules_config', '{ "rules_refus_projet" : {
      "LABEL" : "Refus projet",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--projet" : { "bundle" : "projet" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-validation" ], "value" : "reject" } },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-validation" ], "value" : "reject" } }
      ],
      "DO" : [
        { "node_unpublish" : { "node" : [ "node" ] } },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Nous n\\u2019avons pas pu mettre en ligne votre projet sur La Fabrique des colibris",
            "message" : "Bonjour, \\r\\n\\r\\n[node:field_mod]\\r\\n\\r\\nCordialement, \\r\\nL\\u0027\\u00e9quipe de Colibris",
            "from" : "lafabrique@colibris-lemouvement.org",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_user_suspect'] = entity_import('rules_config', '{ "rules_user_suspect" : {
      "LABEL" : "User suspect",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "DO" : []
    }
  }');
  $items['rules_validation_besoin_competences'] = entity_import('rules_config', '{ "rules_validation_besoin_competences" : {
      "LABEL" : "Validation besoins (voir si le projet reli\\u00e9 est existant)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--need_people" : { "bundle" : "need_people" },
        "node_insert--need_stuff" : { "bundle" : "need_stuff" },
        "node_insert--need_money" : { "bundle" : "need_money" }
      },
      "IF" : [ { "data_is_empty" : { "data" : [ "node:field_projet" ] } } ],
      "DO" : [
        { "mail" : {
            "to" : "florian@colibris-lemouvement.org",
            "subject" : "spam potentiel [node:title]",
            "message" : [ "node:body:value" ],
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_validation_projet'] = entity_import('rules_config', '{ "rules_validation_projet" : {
      "LABEL" : "Validation projet",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--projet" : { "bundle" : "projet" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-validation" ], "value" : "valid" } },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-validation" ], "value" : "valid" } }
      ],
      "DO" : [
        { "node_publish" : { "node" : [ "node" ] } },
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "Votre projet est en ligne sur La Fabrique des colibris !",
            "message" : "Bonjour, \\r\\n\\r\\n\\u00c7a y est, votre projet est en ligne sur La Fabrique des colibris !\\r\\nVous pouvez le visualiser en suivant ce lien : [node:url]\\r\\nParlez-en maintenant autour de vous, sur votre site, par mail, ou sur les r\\u00e9seaux sociaux.\\r\\n\\r\\nSi vous ne l\\u2019avez pas fait au moment de la cr\\u00e9ation de votre projet, vous pouvez ajouter des demandes de mat\\u00e9riel, financement participatif ou ressources humaines en vous connectant \\u00e0 votre compte : [site:url]user \\r\\nPour d\\u00e9ambuler facilement au coeur de La Fabrique, n\\u2019h\\u00e9sitez pas \\u00e0 consulter le guide du porteur de projet ! ([site:url]guide)\\r\\n\\r\\nDes colibris vont bient\\u00f4t vous contacter pour participer \\u00e0 votre projet, tenez vous pr\\u00eat(e) !\\r\\n\\r\\nNous sommes ravis de pouvoir vous soutenir dans la r\\u00e9ussite de votre projet. Merci de nous faire confiance. \\r\\nCordialement, \\r\\nL\\u0027\\u00e9quipe de Colibris\\r\\n",
            "from" : "lafabrique@colibris-lemouvement.org",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_validation_projet_auvergne_rhone_alpes'] = entity_import('rules_config', '{ "rules_validation_projet_auvergne_rhone_alpes" : {
      "LABEL" : "Validation projet auvergne rhone alpes",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "9",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--projet" : { "bundle" : "projet" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-validation" ], "value" : "valid" } },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-validation" ], "value" : "valid" } },
        { "text_matches" : {
            "text" : [ "node:field-proj-adresse:postal-code" ],
            "match" : "(01|03|07|15|26|38|42|43|63|69|73|74)\\\\d{3}",
            "operation" : "regex"
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "florian@colibris-lemouvement.org",
            "subject" : "Un nouveau projet dans votre r\\u00e9gion !",
            "message" : "Bonjour,\\r\\n\\r\\nUn nouveau projet est en ligne sur La Fabrique des colibris !\\r\\nVous pouvez le d\\u00e9couvrir en suivant ce lien : [node:url]\\r\\n\\r\\nB\\u00e9n\\u00e9volat, expertise, pr\\u00eat de mat\\u00e9riel, financement, ... Participez \\u00e0 la r\\u00e9ussite de ce projet en fonction de ses demandes de soutien !\\r\\n\\r\\nEt d\\u00e8s maintenant, parlez-en autour de vous, sur votre site, par \\u00e9mail ou sur les r\\u00e9seaux sociaux.\\r\\n\\r\\nCordialement,\\r\\nL\\u0027\\u00e9quipe de la Fabrique des colibris",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
