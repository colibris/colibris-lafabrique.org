<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function fabrique_specific_blocks_and_menus_user_default_roles() {
  $roles = array();

  // Exported role: Membre.
  $roles['Membre'] = array(
    'name' => 'Membre',
    'weight' => 6,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: non vérifié.
  $roles['non vérifié'] = array(
    'name' => 'non vérifié',
    'weight' => 7,
  );

  // Exported role: webmastering.
  $roles['webmastering'] = array(
    'name' => 'webmastering',
    'weight' => 3,
  );

  return $roles;
}
