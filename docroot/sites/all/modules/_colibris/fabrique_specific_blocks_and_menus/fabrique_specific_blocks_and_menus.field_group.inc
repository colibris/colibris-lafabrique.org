<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fabrique_specific_blocks_and_menus_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_infos|node|projet|form';
  $field_group->group_name = 'group_infos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'projet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Informations',
    'weight' => '1',
    'children' => array(
      0 => 'field_proj_desc',
      1 => 'field_proj_theme',
      2 => 'field_proj_website',
      3 => 'field_accroche',
      4 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_infos|node|projet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mod|node|projet|default';
  $field_group->group_name = 'group_mod';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'projet';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Modération',
    'weight' => '7',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-mod field-group-fieldset',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_mod|node|projet|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mod|node|projet|form';
  $field_group->group_name = 'group_mod';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'projet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Modération',
    'weight' => '11',
    'children' => array(
      0 => 'field_mod',
      1 => 'field_validation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Modération',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-mod field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_mod|node|projet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_name|user|user|default';
  $field_group->group_name = 'group_name';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Prénom Nom',
    'weight' => '1',
    'children' => array(
      0 => 'field_nom',
      1 => 'field_prenom',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Prénom Nom',
      'instance_settings' => array(
        'id' => 'name',
        'classes' => 'name-group',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_name|user|user|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Informations');
  t('Modération');
  t('Prénom Nom');

  return $field_groups;
}
