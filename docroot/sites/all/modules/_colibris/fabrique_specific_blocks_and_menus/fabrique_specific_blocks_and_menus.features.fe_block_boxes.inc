<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function fabrique_specific_blocks_and_menus_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Pas encore de compte ?';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'account_create_info';
  $fe_block_boxes->body = '<p>Créez un compte sur la Fabrique des Colibris pour déposer vos projets. <a class="button btn-block yellow" href="/user/register"><i class="fa fa-pencil"></i> Créer un compte</a></p>
';

  $export['account_create_info'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Introduction Besoins';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'besoinsintro';
  $fe_block_boxes->body = '<p>Vous souhaitez mettre votre énergie, votre expérience, du matériel, ou un peu d\'argent au service de projets inspirants ?</p>
<p class="up">Ça tombe bien,</p>
<h2>DES CENTAINES DE PROJETS N\'ATTENDENT QUE VOUS!</h2>
';

  $export['besoinsintro'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Soutenir la fabrique';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'contribute';
  $fe_block_boxes->body = '<h1>
	<strong>soutenir</strong> la fabrique</h1>
<h3>
	Devenez bénévoles !</h3>
<p><a class="contrib-link" href="moderer-les-projets">Modérer les projets</a> <a class="contrib-link" href="ecrire-les-articles">Écrire les articles</a> <a class="contrib-link" href="developper-la-plateforme">Développer la plateforme</a></p>
';

  $export['contribute'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Introduction FAQ';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'faqintro';
  $fe_block_boxes->body = '<strong>La Fabrique des colibris est une <h2>PLATEFORME EN LIGNE D\'ENTRAIDE CITOYENNE</h2> où chacun peut faire sa part.</strong>
<p>Que vous souhaitiez y déposer votre projet ou contribuer à une initiative près de chez vous, cette rubrique vous permettra de démarrer l\'aventure sur la Fabrique en fonction de vos besoins !</p>
';

  $export['faqintro'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'mode d\'affichage liste projets (liste)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'proj_display_list';
  $fe_block_boxes->body = '<div class="projet-display">
<a href="/liste-des-projets" class="liste active">liste</a>
<a href="/carte-des-projets" class="carte">carte</a>
<h3>Affichage</h3>
</div>';

  $export['proj_display_list'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'mode d\'affichage liste projets (carte)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'proj_display_map';
  $fe_block_boxes->body = '<div class="projet-display">
<a href="/liste-des-projets" class="liste">liste</a>
<a href="/carte-des-projets" class="carte active">carte</a>
<h3>Affichage</h3>
</div>';

  $export['proj_display_map'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Introduction Projets';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'projetsintro';
  $fe_block_boxes->body = '<p class="up">Des centaines de projets participent à la construction</p>
<h2>
	D\'UNE SOCIÉTÉ ÉCOLOGIQUE ET SOLIDAIRE PARTOUT SUR LES TERRITOIRES</h2>
<p>&nbsp;</p>
<p>Partez à leur rencontre et à la découverte de mille et une façons de "faire sa part" à leurs côtés.</p>
';

  $export['projetsintro'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Colonne de droite de la home';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'sidebarhome';
  $fe_block_boxes->body = '<div class="intro">
<h1>ENSEMBLE, ON VA PLUS LOIN !</h1>
Financement, bénévolat,<br />
expertise, prêt de matériel, ...<br />
<strong>Participez à la réussite des projets des colibris !</strong>
<a href="/carte-des-projets" class="map-link">
<img src="sites/all/themes/fabrique/images/map.jpg" alt="carto" />
</a>
<a href="/carte-des-projets" class="big-link projects-map">
<strong>+</strong> de 300 projets<br /><small>à découvrir et à soutenir !</small></a>
</div>
<a href="/liste-des-projets" class="big-link projects"><strong>découvrir</strong><br />les projets</a>
<a href="/aider-les-projets" class="big-link help"><strong>aider</strong><br />les projet</a>';

  $export['sidebarhome'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'tabs projets (demandes)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'tabs_projets_besoins';
  $fe_block_boxes->body = '<p><a class="projets-tabs active" href="/liste-des-demandes">Les demandes</a> <a class="projets-tabs" href="/liste-des-projets">Les projets</a></p>
';

  $export['tabs_projets_besoins'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'tabs projets (projets)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'tabs_projets_projets';
  $fe_block_boxes->body = '<a class="projets-tabs" href="/liste-des-demandes">Les demandes</a> <a class="projets-tabs active" href="/liste-des-projets">Les projets</a>
<div class="projet-display">
<a href="/liste-des-projets" class="liste <?php print arg(0) == \'liste-des-projets\' ? \'active\' : \'\'; ?>">liste</a>
<a href="/carte-des-projets" class="carte <?php print arg(0) == \'carte-des-projets\' ? \'active\' : \'\'; ?>">carte</a>
</div>';

  $export['tabs_projets_projets'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'intro création compte';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'user_create_intro';
  $fe_block_boxes->body = '<p class="rtecenter"><strong>Bonjour et bienvenue sur la Fabrique des colibris !</strong></p>

<p class="rtecenter">Pour déposer votre projet, nous vous invitons à vous connecter à votre compte utilisateur.</p>

<p class="rtecenter">&nbsp;</p>

<p class="rtecenter">&nbsp;</p>
';

  $export['user_create_intro'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Video de presentation de la fabrique pour la home';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'videohome';
  $fe_block_boxes->body = '<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" sandbox="allow-same-origin allow-scripts" src="https://video.colibris-outilslibres.org/videos/embed/0c21aa50-60c9-439d-acb9-9c0923bdf172" frameborder="0" allowfullscreen></iframe>
</div>';

  $export['videohome'] = $fe_block_boxes;

  return $export;
}
