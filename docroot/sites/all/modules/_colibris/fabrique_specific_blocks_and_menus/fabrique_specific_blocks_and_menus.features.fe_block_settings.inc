<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fabrique_specific_blocks_and_menus_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-account_create_info'] = array(
    'cache' => -1,
    'css_class' => 'block-create-account',
    'custom' => 0,
    'machine_name' => 'account_create_info',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'user',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -22,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => 'Pas encore de compte ?',
    'visibility' => 1,
  );

  $export['block-besoinsintro'] = array(
    'cache' => -1,
    'css_class' => 'block-intro',
    'custom' => 0,
    'machine_name' => 'besoinsintro',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'liste-des-demandes
recherche-demandes',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -26,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-contribute'] = array(
    'cache' => -1,
    'css_class' => 'block-contribute',
    'custom' => 0,
    'machine_name' => 'contribute',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>
user
user/*
les-projets
les-projets/*
deposer
deposer-un-projet',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -23,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-faqintro'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'faqintro',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'faq',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -28,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-proj_display_list'] = array(
    'cache' => -1,
    'css_class' => 'block-icones',
    'custom' => 0,
    'machine_name' => 'proj_display_list',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'liste-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -31,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-proj_display_map'] = array(
    'cache' => -1,
    'css_class' => 'block-icones',
    'custom' => 0,
    'machine_name' => 'proj_display_map',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'carte-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -30,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-projetsintro'] = array(
    'cache' => -1,
    'css_class' => 'block-intro',
    'custom' => 0,
    'machine_name' => 'projetsintro',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'liste-des-projets
carte-des-projets
recherche',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -27,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-sidebarhome'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'sidebarhome',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -29,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-tabs_projets_besoins'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'tabs_projets_besoins',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'liste-des-demandes',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 18,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-tabs_projets_projets'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'tabs_projets_projets',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'liste-des-projets
carte-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => -29,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-user_create_intro'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'user_create_intro',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'user',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 15,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-videohome'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'videohome',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -30,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['faq-faq_categories'] = array(
    'cache' => 1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'faq_categories',
    'module' => 'faq',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => -17,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['privatemsg-privatemsg-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'privatemsg-menu',
    'module' => 'privatemsg',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 0,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['privatemsg-privatemsg-new'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'privatemsg-new',
    'module' => 'privatemsg',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 0,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'hidden',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -20,
      ),
      'fabrique' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -29,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => -3,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['views--exp-besoins-page'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-besoins-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'liste-des-demandes
recherche-demandes',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -25,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-besoins-page_1'] = array(
    'cache' => -1,
    'css_class' => 'block-search',
    'custom' => 0,
    'delta' => '-exp-besoins-page_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'liste-des-demandes
recherche-demandes',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -32,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-emails_des_oasis-page'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-emails_des_oasis-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => -12,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-projets-page'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-projets-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'liste-des-projets
carte-des-projets
recherche',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -24,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-projets-page_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-projets-page_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'carte-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => -29,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-projets-page_2'] = array(
    'cache' => -1,
    'css_class' => 'block-search',
    'custom' => 0,
    'delta' => '-exp-projets-page_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'recherche
liste-des-projets
carte-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_sidebar',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -33,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views--exp-projets-page_3'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-projets-page_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => -9,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-actus_home-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'actus_home-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -28,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-faq-faq_recent'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'faq-faq_recent',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 4,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-nodequeue_1-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'nodequeue_1-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -22,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 12,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-nodequeue_1-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'nodequeue_1-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => 'content_bottom',
        'status' => 1,
        'theme' => 'fabrique',
        'weight' => -27,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-projets-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'projets-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/*',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
      'Membre' => 7,
      'non vérifié' => 8,
      'webmastering' => 4,
    ),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 11,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-projets-block_2'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'projets-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'carte-des-projets',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'fabrique' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fabrique',
        'weight' => 10,
      ),
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
