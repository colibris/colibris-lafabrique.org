<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function fabrique_specific_blocks_and_menus_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_mod'.
  $permissions['create field_mod'] = array(
    'name' => 'create field_mod',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_validation'.
  $permissions['create field_validation'] = array(
    'name' => 'create field_validation',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_mod'.
  $permissions['edit field_mod'] = array(
    'name' => 'edit field_mod',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_validation'.
  $permissions['edit field_validation'] = array(
    'name' => 'edit field_validation',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_mod'.
  $permissions['edit own field_mod'] = array(
    'name' => 'edit own field_mod',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_validation'.
  $permissions['edit own field_validation'] = array(
    'name' => 'edit own field_validation',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_mod'.
  $permissions['view field_mod'] = array(
    'name' => 'view field_mod',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_validation'.
  $permissions['view field_validation'] = array(
    'name' => 'view field_validation',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_mod'.
  $permissions['view own field_mod'] = array(
    'name' => 'view own field_mod',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_validation'.
  $permissions['view own field_validation'] = array(
    'name' => 'view own field_validation',
    'roles' => array(
      'administrator' => 'administrator',
      'webmastering' => 'webmastering',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
