<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.default_property_validation_rules.inc
 */

/**
 * Implements hook_default_property_validation_rule().
 */
function fabrique_specific_blocks_and_menus_default_property_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Titre besoin compétence';
  $rule->name = 'need_people_title';
  $rule->property_name = 'title';
  $rule->entity_type = 'node';
  $rule->bundle = 'need_people';
  $rule->validator = 'property_validation_length_validator';
  $rule->settings = array(
    'min' => '',
    'max' => '50',
  );
  $rule->error_message = 'Le titre <em>[value]</em> est trop long';
  $export['need_people_title'] = $rule;

  return $export;
}
