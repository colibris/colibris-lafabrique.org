<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function fabrique_specific_blocks_and_menus_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'admin';
  $context->description = '';
  $context->tag = 'admin';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'administrator' => 'administrator',
        'webmastering' => 'webmastering',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'is-admin',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('admin');
  $export['admin'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'no-side';
  $context->description = 'Right sidebar is hidden';
  $context->tag = 'theme';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'ensemble/acteurs-et-projets-pres-de-chez-soi' => 'ensemble/acteurs-et-projets-pres-de-chez-soi',
        'ensemble/des-projets-pres-de-chez-soi' => 'ensemble/des-projets-pres-de-chez-soi',
        'ensemble/le-projet-oasis/la-carte-des-oasis-en-france' => 'ensemble/le-projet-oasis/la-carte-des-oasis-en-france',
        'ensemble/pres-de-chez-vous' => 'ensemble/pres-de-chez-vous',
      ),
    ),
  );
  $context->reactions = array(
    'theme' => array(
      'title' => 'no-side',
      'subtitle' => '',
    ),
    'theme_html' => array(
      'class' => 'no-side',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Right sidebar is hidden');
  t('theme');
  $export['no-side'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'projets';
  $context->description = 'specific project sidebar';
  $context->tag = 'theme';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'projet' => 'projet',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'agir/les-projets' => 'agir/les-projets',
        'agir/les-projets/*' => 'agir/les-projets/*',
        'node/1497' => 'node/1497',
        'user/*' => 'user/*',
        'user' => 'user',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'theme' => array(
      'title' => 'projets',
      'subtitle' => 'projets',
    ),
    'theme_html' => array(
      'class' => 'projets',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('specific project sidebar');
  t('theme');
  $export['projets'] = $context;

  return $export;
}
