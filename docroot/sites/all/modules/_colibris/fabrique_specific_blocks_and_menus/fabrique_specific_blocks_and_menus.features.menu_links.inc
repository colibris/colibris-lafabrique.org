<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fabrique_specific_blocks_and_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_aider-les-projets:liste-des-demandes.
  $menu_links['main-menu_aider-les-projets:liste-des-demandes'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'liste-des-demandes',
    'router_path' => 'liste-des-demandes',
    'link_title' => 'Aider les projets',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_aider-les-projets:liste-des-demandes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_dcouvrir-les-projets:liste-des-projets.
  $menu_links['main-menu_dcouvrir-les-projets:liste-des-projets'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'liste-des-projets',
    'router_path' => 'liste-des-projets',
    'link_title' => 'Découvrir les projets',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_dcouvrir-les-projets:liste-des-projets',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Aider les projets');
  t('Découvrir les projets');

  return $menu_links;
}
