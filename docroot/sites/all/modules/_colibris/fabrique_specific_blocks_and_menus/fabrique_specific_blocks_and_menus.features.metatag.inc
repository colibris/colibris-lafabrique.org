<?php

/**
 * @file
 * fabrique_specific_blocks_and_menus.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function fabrique_specific_blocks_and_menus_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: taxonomy_term.
  $config['taxonomy_term'] = array(
    'instance' => 'taxonomy_term',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[term:name] | [current-page:pager][site:name]',
      ),
      'description' => array(
        'value' => '[term:description]',
      ),
    ),
  );

  return $config;
}
