<?php

/**
 * Implementation of hook_permission().
 */
function social_permission() {
  return array(
    'administer social buttons' => array(
      'title' => t('Administer social buttons'),
      'description' => t('Perform administration tasks for social buttons.'),
    ),
    'view social buttons' => array(
      'title' => t('View social buttons'),
      'description' => t('Display social buttons on nodes.'),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function social_menu() {
  $items['admin/config/social'] = array(
    'title' => 'Social',
	'description' => 'Manage how your site interacts with Social Networks',
	'position' => 'right',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
	'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/social/social-buttons'] = array(
    'title' => 'Social Buttons Integration',
    'description' => 'Administer Social Buttons',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('social_admin_settings'),
	  'access arguments' => array('administer social buttons'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of system_settings_form().
 */
function social_admin_settings($form, $form_state) {
  $nodeTypes = node_type_get_types();
  $options = array();
  foreach ($nodeTypes as $k => $v) {
    $options[$k] = $v->name;
  }
  $form['social_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#description' => t('Which content types to enable the re-tweet and facebook like buttons for.'),
    '#default_value' => variable_get('social_nodes', array()),
    '#options' => $options,
  );
  $form['social_node_type'] = array(
    '#type' => 'radios',
    '#title' => t('Node Type'),
    '#description' => t('The type of node that the buttons should be displayed on.'),
    '#default_value' => variable_get('social_node_type', 'full'),
    '#options' => array('full' => 'Full Node', 'teaser' => 'Teaser', 'both' => 'Full Nodes and Teasers'),
  );
  $form['social_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The label for the social buttons on the node itself. Leave blank for no label. Displays in an H2 tag.'),
    '#default_value' => variable_get('social_label', 'Social'),
  );
  $form['social_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Button Type'),
    '#description' => t('Chose the type of button to use'),
    '#default_value' => variable_get('social_button_type', 'small'),
    '#options' => array('small' => 'Button Only', 'medium' => 'Button + Count on Side', 'large' => 'Button + Count Above'),
  );
  $form['social_facebook_colorscheme'] = array(
    '#type' => 'radios',
    '#title' => t('Facebook Color Scheme'),
    '#description' => t('The color scheme for the facebook like button'),
    '#default_value' => variable_get('social_facebook_colorscheme', 'light'),
    '#options' => array('dark' => 'Dark', 'light' => 'Light'),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_node_view().
 */
function social_node_view($node, $view_mode = 'full', $langcode = NULL){
	$contentTypes = variable_get('social_nodes', array());
	$nodeTypes = variable_get('social_node_type', 'full');


	$content_image = false;
 	if(isset($node->field_image['und'])) {
	 	$content_image = image_style_url('fb', $node->field_image['und'][0]['uri']);
	}
 	if(isset($node->field_proj_photos['und'])) {
	 	$content_image = image_style_url('fb', $node->field_proj_photos['und'][0]['uri']);
	}
	if($content_image) {
		$og_tag = array(
			'#tag' => 'meta',
			'#attributes' => array(
				'property' => 'og:image',
				'content' => $content_image
			)
		);
		drupal_add_html_head($og_tag,'ogimage');
	}
  if(isset($contentTypes[$node->type])
    && $contentTypes[$node->type] != null
		&& ($nodeTypes == $view_mode || $nodeTypes == 'both')
		&& user_access('view social buttons')
		&& (isset($node->body['fr']) || isset($node->body['und']) || isset($node->field_accroche['und'][0]['value']))) {

    $url = url(drupal_lookup_path('alias', 'node/' . $node->nid), array('absolute' => TRUE));
    if(!$url) { $url = url('node/'.$node->nid, array('absolute' => TRUE)); }
    $url_encoded = urlencode($url);

	if (isset($node->body) && isset($node->body['fr'][0]['safe_summary'])) {
		$body = $node->body['fr'][0]['safe_summary'];
	} else if (isset($node->body['und']) && $node->body['und'][0]['safe_summary']) {
		$body = $node->body['und'][0]['safe_summary'];
	}else {
		$body = '';
	}

    $button_type = variable_get('social_button_type', 'small');
    if($button_type == 'small'){
      $facebook_btn = 'standard';
      $twitter_btn = 'none';
      $google_btn = 'medium';
      $annotation = 'none';
    }
    elseif($button_type == 'medium'){
      $facebook_btn = 'button_count';
      $twitter_btn = 'horizontal';
      $google_btn = 'medium';
      $annotation = '';
    }
    elseif($button_type == 'large'){
      $facebook_btn = 'box_count';
      $twitter_btn = 'vertical';
      $google_btn = 'tall';
      $annotation = '';
    }


	$params = array(
		'title' => $node->title,
		'summary' => strip_tags($body),
		'url' => $url,
		'short' => '',
		'image' => ''
	);

	if(isset($node->field_accroche['und'])) {
		$params['summary'] = $node->field_accroche['und'][0]['value'];
	}


	if(isset($content_image)) {
		$params['contentImage'] = $content_image;
	}

//	drupal_add_js('http://static.ak.fbcdn.net/connect.php/js/FB.Share','external');
	drupal_add_js(array('social' => $params), 'setting');

	$output = '<div class="social-btns">';
	$output .= '<a href="#" class="tw"><i class="fa fa-twitter"></i></a>';
	$output .= '<a href="#" class="fb" name="fb_share" type="icon"><i class="fa fa-facebook"></i></a>';
	$output .= '<a href="#" class="mastodon"><i class="fa fa-mastodon"></i></a>';
	$output .= '<a href="#" class="diaspora"><i class="fa fa-diaspora"></i></a>';
	$output .= '</div>';

    $node->content['social-bottom'] = array(
      '#markup' => $output,
      '#weight' => 100
    );
  }
}
