(function ($) {
  jQuery('.fb').click(function(e){
    var data = Drupal.settings.social;
    var url  = 'https://www.facebook.com/sharer.php?s=100'
    + '&p[title]=' +  encodeURIComponent(data.title)
    + '&p[summary]=' +  encodeURIComponent(data.summary)
    + '&p[url]=' + encodeURIComponent(data.url)


    if (data.contentImage) {
      url += '&p[images][0]='
      url += encodeURIComponent(data.image);
      url += '&p[images][1]='
      url += encodeURIComponent(data.contentImage);
    } else {
      url += '&p[images][0]='
      url += encodeURIComponent(data.image);
    }

    //console.log(url);

    window.open(url,'sharer','toolbar=0,status=0,width=548,height=325');
    return false;
  })

  jQuery('.tw').on('click', function(e){
    var data = Drupal.settings.social;
    var url  = 'https://twitter.com/share'
    + '?url=' + encodeURIComponent(data.url)
    + '&counturl=' +  encodeURIComponent(data.url)
    + '&text=' +  encodeURIComponent(data.title)
    console.log(url);
    window.open(url,'sharer','toolbar=0,status=0,width=548,height=325');
    return false;
  })

  jQuery('.mastodon').click(function(e){
    var data = Drupal.settings.social;
    var url  = 'https://framapiaf.org/share?title=' + encodeURIComponent(data.title)
    + '&url=' +  encodeURIComponent(data.url)

    window.open(url,'sharer','toolbar=0,status=0,width=548,height=325');
    return false;
  })

  jQuery('.diaspora').click(function(e){
    var data = Drupal.settings.social;
    var url  = 'https://share.diasporafoundation.org/?'
    + 'title=' + encodeURIComponent(data.title)
    + '&url=' + encodeURIComponent(data.url)

    window.open(url,'sharer','toolbar=0,status=0,width=548,height=325');
    return false;
  })
})(jQuery);
