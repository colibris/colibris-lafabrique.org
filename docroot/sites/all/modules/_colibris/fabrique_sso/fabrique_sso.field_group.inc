<?php

/**
 * @file
 * fabrique_sso.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fabrique_sso_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_name|user|user|default';
  $field_group->group_name = 'group_name';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Prénom Nom',
    'weight' => '2',
    'children' => array(
      0 => 'field_nom',
      1 => 'field_prenom',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Prénom Nom',
      'instance_settings' => array(
        'id' => 'name',
        'classes' => 'name-group',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_name|user|user|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Prénom Nom');

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_desc|node|projet|default';
  $field_group->group_name = 'group_user_desc';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'projet';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'A propos de moi',
    'weight' => '0',
    'children' => array(
      0 => 'field_user_tel',
      1 => 'field_user_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'A propos de moi',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-user-desc field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_user_desc|node|projet|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_desc|node|projet|form';
  $field_group->group_name = 'group_user_desc';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'projet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'A propos de moi',
    'weight' => '0',
    'children' => array(
      0 => 'field_user_tel',
      1 => 'field_user_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-user-desc field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_user_desc|node|projet|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('A propos de moi');

  return $field_groups;
}
