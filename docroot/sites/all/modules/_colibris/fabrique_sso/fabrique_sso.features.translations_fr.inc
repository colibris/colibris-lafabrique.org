<?php

/**
 * @file
 * fabrique_sso.features.translations_fr.inc
 */

/**
 * Implements hook_translations_fr_defaults().
 */
function fabrique_sso_translations_fr_defaults() {
  $translations = array();
  $translatables = array();
  $translations['fr:default']['0432acd3669853a47c8d83289fecb6a9'] = array(
    'source' => 'Your profile has been updated.',
    'context' => '',
    'location' => '/cas?refresh=profile',
    'translation' => 'Votre profil a été mis à jour.',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Your profile has been updated.', array(), array('context' => ''));
  $translations['fr:default']['86e7aab96e23c67144bb4be3fe40d161'] = array(
    'source' => 'Logged in via CAS as %cas_username.',
    'context' => '',
    'location' => '/cas',
    'translation' => 'Connexion réussie %cas_username !',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Logged in via CAS as %cas_username.', array(), array('context' => ''));
  return $translations;
}
