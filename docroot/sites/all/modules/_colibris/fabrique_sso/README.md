## Fabrique_sso module

### Installation

First, verify that /sites/all/librairies/CAS has been pulled from the git repository.

And execute :

- ```drush en fabrique_sso```
- ```drush cc all``` (clear all caches)

### Migration

At installation, the module process the data migration for all users : *'field_user_tel'* and *'field_user_desc'* are moved to the *'project'* entity.
 
If needed, you can execute it again by typing :

- ```drush ev "include 'includes/install.inc'; drupal_set_installed_schema_version('fabrique_sso', 0)"```
- ```drush updatedb```

Then verify the confirmation log messages with :\
```drush ws --count=100```  (with 100 the number of the last comments showed)\
Or during the migration, you can see the logs in realtime by executing in another terminal :\
```drush ws --tail```

___Trick___ :
To verify the version of the module, you can type :
```drush ev "include 'includes/install.inc'; echo drupal_get_installed_schema_version('fabrique_sso')"```. \
If the migration has been processed, this should return *'7101'*.

### After...

When you are sure that everything is working, you will be able delete the fields *'field_user_tel'* and *'field_address'* of the user account with the administration webpages. Caution, you will not be able to recover these fields afterward. 
