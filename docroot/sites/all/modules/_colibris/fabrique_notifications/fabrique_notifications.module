<?php

define('DEFAULT_IFRAME_URL', 'https://alertes.colibris-lafabrique.org');

/**
 * @file
 * Code for La Fabrique notifications feature.
 */

/**
 * Implementation of hook_help()
 *
 * Define the help section of the module
 */
function fabrique_notifications_help($path, $arg){
  switch ($path) {
    case 'admin/help#fabrique_notifications':
      return '<p>' . t('Le module notifications de La Fabrique permet de servir à l\'adresse <i>https://{website}/notifications</i> une page qui intègre un iframe d\'un autre site.') . '</p>' .
        '<p>' . t('L\'interface d\'administration du module se trouve dans l\'espace d\'administration : onglet « Configuration », rubrique « Interface utilisateur »') . '</p>';
  }
}

/**
 * Implements hook_menu().
 *
 * Define the link to "Gérer mes notification" page
 *
 */
function fabrique_notifications_menu(){

  $items['admin/config/user-interface/notifications'] = array(
    'title' => t('Notifications La Fabrique'),
    'description' => t('Configuration du module notifications de La Fabrique'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fabrique_notifications_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['notifications'] = array(
    'title' => 'Gérer mes notifications',
    'page callback' => 'fabrique_notifications_serve_iframe',
    'access callback' => true,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Define the notifications administration form
 */
function fabrique_notifications_admin_settings(){

  $form['fabrique_webhook_iframe'] = array(
    '#type' => 'fieldset',
    '#title' => t('Iframe à intégrer'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fabrique_webhook_iframe']['fabrique_notifications_iframe_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Url de la page'),
    '#description' => t('Adresse absolue de la page de gestion des notifications.'),
    '#default_value' => variable_get('fabrique_notifications_iframe_url', DEFAULT_IFRAME_URL),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_alter()
 */
function fabrique_notifications_form_alter(&$form, &$form_state, $form_id){
  if ($form_id=='fabrique_notifications_admin_settings') {
    $form['#validate'][]='fabrique_notifications_admin_form_validate';
    return $form;
  }
}

/**
 * Check if the admin form has an URL in 'service_url' form
 */
function fabrique_notifications_admin_form_validate($form, &$form_state){
  $iframeUrl = $form_state['values']['fabrique_notifications_iframe_url'];
  if (empty($iframeUrl) || !filter_var($iframeUrl, FILTER_VALIDATE_URL))
    form_set_error('fabrique_notifications_iframe_url', t('Merci de renseigner une « Url de la page » valide'));
}

/**
 * Display the 'Gérer mes notifications' page which includes this configuration page in iframe
 */
function fabrique_notifications_serve_iframe() {

  // get the iframe url defined in the configuration page
  $iframeUrl = variable_get('fabrique_notifications_iframe_url', DEFAULT_SERVICE_URL);

  // Deny access to anonymous users
  if( !user_is_logged_in() ) return MENU_ACCESS_DENIED;

  // get the values to set in the GET params
  $account = user_load($GLOBALS['user']->uid);
  $uuid = $GLOBALS['user']->uuid ?? null;
  $email = $account->mail ?? null;

  // add the GET param to the url
  $separator = strpos($iframeUrl, '?') ? '&' : '?';
  if (isset($uuid)) {
    $iframeUrl = $iframeUrl . $separator . 'id=' . $uuid;
    $separator = '&';
  }
  if (isset($email)) {
    $iframeUrl = $iframeUrl . $separator . 'email=' . urlencode($email);
  }

  return "<style>
      iframe {
        width: 1px;
        min-width: 100%;
        border-width: 0px;
      }
    </style>
    <iframe class='auto-resize' src='" . $iframeUrl . "'></iframe>
    <script>
      iFrameResize({ log: true }, '#notifications')
    </script>";
}
