## Fabrique notifications module

The notifications module serves a webpage at the https://{website}/notifications address which integrate an iframe from another site.

### Installation

This module depends on the  *iframe_resizer* module.
First, verify that `/sites/all/librairies/iframe-resizer` and `/sites/all/modules/iframe-resizer` has been pulled from the git repository.

Then install the module *iframe_resizer* with the administration tool `drush` by taping:
- `drush en iframe_resizer -y`
- `drush cc all` (clear all caches)

One installed, configure this module by accessing the configuration page in the administration space : 'Configuration' tab, 'Interface utilisateur' section, 'iFrame Resizer' link.
As showed in the following screenshot, you have to check 'This site hosts resizable iFrames.', then select 'Specific iFrames' and write '.auto-resize' in the text field below.
![](screenshot_config_iframeresizer.png)

And to finish, you can install the *fabrique_notifications* module :
- `drush en fabrique_notifications -y`
- `drush cc all` (clear all caches)

### Configuration

The module has an web interface to configure the url of the embedded iframe. Please find it the administration space: 'Configuration' tab, 'Interface utilisateur' section.
It's called 'Notifications La Fabrique'.

### Page being embedded in iframe

So that the page referenced by the configuration field 'Url de la page' be property display through the iframe, it has to include the javascript file 'iframeResizer.contentWindow.min.js'.
The file need also to be the same version that the library hosted by this instance of drupal. Thus, you can easily get it in the directory `sites/all/librairies/iframe-resizer/js/`.

Two GET params are given to the iframe page : *id* and *email*, the first corresponds to the UID of the CAS server, and the second the email user.
The embedded page can then easily use these params.
