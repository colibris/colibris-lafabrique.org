(function ($) {
	Drupal.behaviors.ProjetsContact = {
		attach: function (context, settings) {
			var values = '';
			if(Drupal.settings.ProjetsContact.pnid.length) {
				values += Drupal.settings.ProjetsContact.pnid;
			}
			if(Drupal.settings.ProjetsContact.nnid.length) {
				values += '/' + Drupal.settings.ProjetsContact.nnid;
			}
			$('.webform-component--data input').val(values);
		}
	};
})(jQuery);
