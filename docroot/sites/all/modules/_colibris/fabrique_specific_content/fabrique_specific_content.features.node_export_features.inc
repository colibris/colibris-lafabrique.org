<?php

/**
 * @file
 * fabrique_specific_content.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function fabrique_specific_content_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => \'youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada \',
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada youpi pouloum toto dada\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => \'200\',
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Actualité trois\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'actualit_s\',
      \'uid\' => \'1086\',
      \'uuid\' => \'2c435efc-be43-4b9b-a84e-fddf1f10a3dd\',
      \'vid\' => NULL,
      \'vuuid\' => \'15d0d1d4-5029-4d65-974a-7543301ea9a2\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => "tretr ret ret ret ert ret ert  rte ret ret retret \\r\\ndsfdsf dsf dsf dsf s\\r\\nf dsf dsf ds gdf\\r\\nhdgh fgh gjgjg;lkh;lkrtl;ret ;klret ert ret  ret ret ",
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => "tretr ret ret ret ert ret ert  rte ret ret retret \\ndsfdsf dsf dsf dsf s\\nf dsf dsf ds gdf\\nhdgh fgh gjgjg;lkh;lkrtl;ret ;klret ert ret  ret ret",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => \'200\',
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Actualité de départ\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'actualit_s\',
      \'uid\' => \'1086\',
      \'uuid\' => \'567a0762-e112-4bc5-80c1-a47f6c6c4bf4\',
      \'vid\' => NULL,
      \'vuuid\' => \'b10bb80d-2e2e-4078-a423-2633b879dc32\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => \' fdsf dksf;l dskf ;ldwkf ;lewrkew;l dms,fmds.,fmd.s,fmdfewfmewklfm.ds,fmds.,fmds.  dfsdsmf.d fm,ds.f,dsmf .d,fm ds.,fm dsfs\',
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'fdsf dksf;l dskf ;ldwkf ;lewrkew;l dms,fmds.,fmd.s,fmdfewfmewklfm.ds,fmds.,fmds.  dfsdsmf.d fm,ds.f,dsmf .d,fm ds.,fm dsfs\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'field_categorie\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'113\',
          ),
        ),
      ),
      \'field_detailed_question\' => array(
        \'und\' => array(
          array(
            \'value\' => \'sdfsd ff dsf dfsdf d;kslf;dls fkl;dsl k;fk;ldfkl;l;kl; fdsl;f kds;flkewr;lewm.,xnxz.mvds tew fdssk;l\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'sdfsd ff dsf dfsdf d;kslf;dls fkl;dsl k;fk;ldfkl;l;kl; fdsl;f kds;flkewr;lewm.,xnxz.mvds tew fdssk;l\',
          ),
        ),
      ),
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => 200,
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'1\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Pourquoi tant de haine ?\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'faq\',
      \'uid\' => \'1086\',
      \'uuid\' => \'73da7aa8-6d64-4987-a884-2647a8866b7c\',
      \'vid\' => NULL,
      \'vuuid\' => \'0192c490-d0ab-4cf1-866a-ba0291fbe462\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => "<p>&nbsp;rew rewr ewr ew r</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p> rew rewr ewr ew r</p>\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'field_categorie\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'113\',
          ),
        ),
      ),
      \'field_detailed_question\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>fsd fewfds ewrew rew rewr</p>\\r\\n",
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p>fsd fewfds ewrew rew rewr</p>\',
          ),
        ),
      ),
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => 200,
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'1\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Découvrir les projets\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'faq\',
      \'uid\' => \'1086\',
      \'uuid\' => \'986beba0-4dff-4747-b2d8-4f42e05e64bf\',
      \'vid\' => NULL,
      \'vuuid\' => \'f214da1d-99c8-4e1b-a99a-fd88750a04a7\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => "<p>t er tret ert ret ret ret re tret ret ret rew hnhvcbnxfcxvfdbvxcvb</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p>t er tret ert ret ret ret re tret ret ret rew hnhvcbnxfcxvfdbvxcvb</p>\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'field_categorie\' => array(
        \'und\' => array(
          array(
            \'tid\' => \'112\',
          ),
        ),
      ),
      \'field_detailed_question\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>dfgger gert ret reter</p>\\r\\n",
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p>dfgger gert ret reter</p>\',
          ),
        ),
      ),
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => 200,
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'1\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'question test florian\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'faq\',
      \'uid\' => \'1086\',
      \'uuid\' => \'a81645dc-d8fd-4177-8419-8955d3eb2a56\',
      \'vid\' => NULL,
      \'vuuid\' => \'acc670cb-240b-4286-92fd-f70b159b4cea\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => \'lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus \',
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus lorem ipsus\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => \'200\',
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Actualité deux\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'actualit_s\',
      \'uid\' => \'1086\',
      \'uuid\' => \'d2414a04-6569-48c7-99c6-944dcc58dc2a\',
      \'vid\' => NULL,
      \'vuuid\' => \'6ad39a23-fe5d-4209-abdd-5fe408c7a963\',
    ),
  (object) array(
      \'base_type\' => \'node\',
      \'body\' => array(
        \'fr\' => array(
          array(
            \'value\' => "<p>lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum...</p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p>lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum...</p>\',
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => \'a:6:{s:16:"ckeditor_default";s:1:"t";s:20:"ckeditor_show_toggle";s:1:"t";s:14:"ckeditor_width";s:4:"100%";s:13:"ckeditor_lang";s:2:"en";s:18:"ckeditor_auto_lang";s:1:"t";s:7:"overlay";i:1;}\',
      \'field_categorie\' => array(),
      \'field_detailed_question\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>lorem ipsum ...</p>\\r\\n",
            \'format\' => \'simplified\',
            \'safe_value\' => \'<p>lorem ipsum ...</p>\',
          ),
        ),
      ),
      \'files\' => array(),
      \'internal_nodes\' => array(
        \'source\' => \'content_type\',
        \'action\' => 200,
        \'url\' => \'\',
      ),
      \'language\' => \'fr\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Florian Schmitt\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'1\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1086\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'Pourquoi participer à un projet de la Fabrique ?\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'faq\',
      \'uid\' => \'1086\',
      \'uuid\' => \'fa4dec1f-d2bd-4fb7-9e71-d2c76c85abb2\',
      \'vid\' => NULL,
      \'vuuid\' => \'7dbc1de2-342e-4e75-9200-6bdf1a3fafec\',
    ),
)',
);
  return $node_export;
}
