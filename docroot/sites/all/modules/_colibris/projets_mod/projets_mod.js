(function ($) {
  Drupal.behaviors.ProjetsMod = {
    attach: function (context, settings) {
      $('.vertical-tabs').hide();
      console.log('mod');


    $('.group-mod').css('background','#BEF');
    $('.node-projet-form').append($('.group-mod,.form-actions'));
    $('.node-projet-form > div').eq(0).before('<a href="#" class="show-form" href="#">modifier les champs du projet</a>');
    $('.node-projet-form > div').eq(0).hide();

    $('.show-form').click(function(){
      $('.node-projet-form > div').eq(0).toggle();
    });

    $('.field-name-field-mod').hide();
    if($('input[name="field_validation[und]"]').val() == 'reject') {
        $('.field-name-field-mod').show();
    }
      
    $('input[name="field_validation[und]"]').change(function() {
        var modState = $(this).val();
        if(modState == 'reject') {
          $('.field-name-field-mod').show();
        } else {
          $('.field-name-field-mod').hide();
          $('textarea[name="field_mod[und][0][value]"]').val('');
        }
    });

    messages = '<div class="preset">En effet, celui-ci n’est pas cohérent avec les valeurs et l’éthique de Colibris et/ou ne correspond pas avec les propositions faites sur La Fabrique.</div>'
      + '<div class="preset">Malheureusement, nous n’avons pas pu mettre en ligne votre projet sur La Fabrique des Colibris.\n\nNous vous invitons à lire les principes de fonctionnement de La Fabrique dans la rubrique "Comment ça marche ?\n\nNous sommes disponibles pour vous en dire plus sur les raisons de notre refus.\n\nMerci de votre compréhension !</div>'
      + '<div class="preset">et un autre message par défaut</div>';
    $('#edit-field-mod').append(messages);
    $('.preset').each(function(){
        $(this).css({'cursor':'pointer','background':'#EEE','margin':'3px','font-size':'.9em'});
        $(this).click(function(){
            $('textarea[name="field_mod[und][0][value]"]').val($(this).html());
        });
      
    });
    }
  };
})(jQuery);
