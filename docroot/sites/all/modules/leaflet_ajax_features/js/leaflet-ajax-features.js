(function ($) {

  $(document).bind('leaflet.map', function (e, settings, lMap) {
    var mapId = settings.map_id;

    // Find the matched map in order to make the ajax call.
    $(Drupal.settings.leaflet).each(function () {
      if (this.mapId === mapId) {
        var mapContainer = $('#' + mapId);
        $.ajax({
          url: this.url,
          method: "GET",
          dataType: "json",
          error: function (xhr, status) {
            Drupal.leafletAjax.displayError(mapContainer);
          },
          success: function (data) {
            Drupal.leafletAjax.addFeatures(lMap, data, mapContainer);
          }
        });

      }
    });
  });

  Drupal.leafletAjax = Drupal.leafletAjax || {};

  Drupal.leafletAjax.addFeatures = function (lMap, features, mapContainer) {
    if (features.length > 0) {
      var i;
      for (i = 0; i < features.length; i++) {
        var feature = features[i];
        var lFeature;

        // dealing with a layer group
        if (feature.group) {
          var lGroup = new L.LayerGroup();
          for (var groupKey in feature.features) {
            var groupFeature = feature.features[groupKey];
            lFeature = Drupal.leafletAjax.createFeature(groupFeature, lMap);
            if (groupFeature.popup) {
              lFeature.bindPopup(groupFeature.popup);
            }
            lGroup.addLayer(lFeature);

            // Allow others to do something with the feature within a group.
            $(document).trigger('leaflet.feature', [lFeature, feature]);
          }

          // @todo - workout best way of adding to layer control.

          lMap.addLayer(lGroup);
        }
        else {
          lFeature = Drupal.leafletAjax.createFeature(feature, lMap);
          lMap.addLayer(lFeature);

          if (feature.popup) {
            lFeature.bindPopup(feature.popup);
          }

          // Allow others to do something with the feature.
          $(document).trigger('leaflet.feature', [lFeature, feature]);
        }
      }

      Drupal.leaflet.fitbounds(lMap);

      // Remove the loading container.
      mapContainer.next('.loading-wrapper').fadeOut(1000);
    }
    else {
      Drupal.leafletAjax.displayError(mapContainer);
    }
  };

  Drupal.leafletAjax.createFeature = function (feature, lMap) {
    var lFeature;
    switch (feature.type) {
      case 'point':
        lFeature = Drupal.leaflet.create_point(feature, lMap);
        break;
      case 'linestring':
        lFeature = Drupal.leaflet.create_linestring(feature, lMap);
        break;
      case 'polygon':
        lFeature = Drupal.leaflet.create_polygon(feature, lMap);
        break;
      case 'multipolyline':
        feature.multipolyline = true;
        // no break;
      case 'multipolygon':
        lFeature = Drupal.leaflet.create_multipoly(feature, lMap);
        break;
      case 'json':
        lFeature = Drupal.leaflet.create_json(feature.json, lMap);
        break;
      case 'popup':
        lFeature = Drupal.leaflet.create_popup(feature, lMap);
        break;
      case 'circle':
        lFeature = Drupal.leaflet.create_circle(feature, lMap);
        break;
      case 'circlemarker':
        lFeature = Drupal.leaflet.create_circlemarker(feature, lMap);
        break;
      case 'rectangle':
        lFeature = Drupal.leaflet.create_rectangle(feature, lMap);
        break;
    }

    // assign our given unique ID, useful for associating nodes
    if (feature.leaflet_id) {
      lFeature._leaflet_id = feature.leaflet_id;
    }

    var options = {};
    if (feature.options) {
      for (var option in feature.options) {
        options[option] = feature.options[option];
      }
      lFeature.setStyle(options);
    }

    return lFeature;
  };

  Drupal.leafletAjax.displayError = function (mapContainer) {
    var html = '<p>' + Drupal.t('There was an error loading the features.') + '</p>';
    mapContainer.next('.loading-wrapper').html(html)
  }

})(jQuery);