<?php

/**
 * @file
 * Module file for the Leaflet Ajax Views module.
 */

/**
 * Implements hook_views_api().
 */
function leaflet_ajax_features_views_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'leaflet_ajax_features_views'),
  );
}

/**
 * Implements hook_menu().
 */
function leaflet_ajax_features_views_menu() {
  $items = array();

  // Returns an array of features for a given field within an entity.
  $items['leaflet-ajax-features-views/%/%/%'] = array(
    'page callback' => 'leaflet_ajax_features_views_get_features',
    'page arguments' => array(1, 2, 3),
    'access callback' => TRUE,
    'delivery callback' => 'drupal_json_output',
  );

  return $items;
}

/**
 * Returns the results of a given view processed for display within a leaflet map.
 *
 * @param string $view_name
 *   The type of entity the field is attached to.
 * @param int $view_display
 *   The ID of the entity the field ia attached to.
 *
 * @return array
 */
function leaflet_ajax_features_views_get_features($view_name, $view_display, $field_name) {
  // Early return if some of the information isn't available.
  if (empty($view_name) || empty($view_display)) {
    return array();
  }

  $features = &drupal_static(__FUNCTION__);

  $args = drupal_get_query_parameters();
  $has_args = isset($args['args']);

  // Generate a unique cache ID for the results of this view.
  if ($has_args) {
    // Because the view can be used across different pages with separate
    // arguments affecting the results, we add the arguments to the cache ID.
    $cid = "leaflet_ajax_features_views:$view_name:$view_display:" . str_replace(',', ':', $args['args']);
  }
  else {
    $cid = "leaflet_ajax_features_views:$view_name:$view_display";
  }

  if (!isset($features)) {
    if ($cache = cache_get("leaflet_ajax_features_views:$view_name:$view_display")) {
      $features = $cache->data;
    }
    else {
      $features = array();

      $view = views_get_view($view_name);
      $view->set_display($view_display);
      $view->pre_execute();
      $view->execute();

      $display = $view->style_plugin;
      $options = $display->options;

      $name_field = empty($options['name_field']) ? NULL : $options['name_field'];
      $display->render_fields($view->result);

      foreach ($view->result as $id => $result) {
        $geofield = $display->get_field_value($id, $field_name);
        if (!empty($geofield)) {
          if (is_object($result)) {
            if (!empty($result->{$display->entity_info['entity keys']['id']})) {
              $entity_id = $result->{$display->entity_info['entity keys']['id']};
            }
            elseif (isset($result->entity)) {
              $entity_id = $result->entity;
            }
            elseif (isset($result->_field_data)) {
              $tmp = $result->_field_data;
              $tmp = array_pop($tmp);
              $entity_id = $tmp['entity']->{$display->entity_info['entity keys']['id']};
            }
            $entity = entity_load_single($display->entity_type, $entity_id);
            if ($options['description_field'] === '#rendered_entity') {
              $build = entity_view($display->entity_type, array($entity), $options['view_mode']);
              $description = drupal_render($build);
            }
            // Normal rendering via fields:
            else {
              $description = '';
              if ($name_field) {
                $description = $display->rendered_fields[$id][$name_field];
              }
              if ($options['description_field']) {
                $description .= $display->rendered_fields[$id][$options['description_field']];
              }
            }
          }

          $points = leaflet_process_geofield($geofield);

          // Attach pop-ups if we have rendered into $description:
          if (isset($description)) {
            foreach ($points as &$point) {
              $point['popup'] = $description;
            }
          }

          // Attach also titles & entities, they might be used later on.
          if ($name_field) {
            foreach ($points as &$point) {
              if (isset($display->rendered_fields[$id][$name_field])) {
                $point['label'] = htmlspecialchars_decode(strip_tags($display->rendered_fields[$id][$name_field]));
              }
            }
          }

          if ($options['icon']['iconType'] == 'html') {
            foreach ($points as &$point) {
              $target_field = $options['icon']['html'];
              $point['rendered_html'] = isset($display->rendered_fields[$id][$target_field]) ? $display->rendered_fields[$id][$target_field] : '';
            }
          }

          $vector_settings = FALSE;
          if (!empty($options['vector_display']) && $options['vector_display']['stroke_override']) {
            $vector_settings = TRUE;
          }

          // Apply vector display settings:
          if ($vector_settings) {
            foreach ($points as &$point) {
              if ($point['type'] != 'point') {
                // To avoid overwrite the options with empty values removes all
                // NULL, FALSE and empty Strings and leaves zero values.
                $style_options = array_filter($options['vector_display'], 'strlen');
                unset($style_options['stroke_override']);
                $point['options'] = $style_options;
              }
            }
          }

          drupal_alter('leaflet_views_alter_points_data', $result, $points);
          // Merge these points into the $data array for map rendering:
          $features = array_merge($features, $points);
        }
      }

      cache_set($cid, $features, 'cache');
    }
  }

  return $features;
}
