<?php

/**
 * @file
 * Provides a Views display plugin to load features via AJAX.
 */

class leaflet_ajax_features_views_plugin_style extends leaflet_views_plugin_style {

  /**
   * Renders view.
   */
  function render() {
    if (!empty($this->view->live_preview)) {
      return t('No preview available.');
    }
    $data = array();
    $map = leaflet_map_get_info($this->options['map']);
    // Is there a geofield selected?
    if ($this->options['data_source']) {
      $this->render_fields($this->view->result);

      $entity_type = isset($this->entity_type) ? $this->entity_type : '';
      leaflet_apply_map_settings($map, $data, $this->options, $entity_type);
      if (empty($data) && !empty($this->options['hide_empty'])) {
        return;
      }

      return array(
        '#theme' => 'leaflet_ajax_features_map',
        '#settings' => $this->options,
        '#map_type' => 'view',
        '#url_settings' => array(
          'entity_type' => $entity_type,
          'view_name' => $this->view->name,
          'view_display' => $this->view->current_display,
          'field_name' => $this->options['data_source'],
          'args' => $this->view->args,
        ),
      );
    }

    return;
  }

}