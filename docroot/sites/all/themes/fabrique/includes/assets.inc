<?php

/**
 * @file
 * Handle JS inclusion or exclusion.
 */
function fabrique_js_alter(&$js)
{

    // Move all scripts to the footer.
    // To force a file to go in the header do -
    // drupal_add_js('path/to/js', array('force_header' => TRUE));
    // drupal_add_library('system', 'jquery.form');
    // drupal_add_library('system', 'drupal.ajax');
    foreach ($js as &$item) {
        if (empty($item['force_header'])) {
            $item['scope'] = 'footer';
        }
    }
}
