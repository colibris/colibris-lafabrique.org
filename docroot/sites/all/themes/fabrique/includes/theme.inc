<?php

// /**
//  *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
//  */
// function colibris_menu_local_tasks(&$vars) {
//   $output = '';

//   if (!empty($vars['primary'])) {
//     $vars['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
//     $vars['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
//     $vars['primary']['#suffix'] = '</ul>';
//     $output .= drupal_render($vars['primary']);
//   }

//   if (!empty($vars['secondary'])) {
//     $vars['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
//     $vars['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
//     $vars['secondary']['#suffix'] = '</ul>';
//     $output .= drupal_render($vars['secondary']);
//   }

//   return $output;
// }

// function colibris_menu_link(array $vars) {
//   $element = $vars['element'];
//   $element['#attributes']['class'][] = 'menu-' . $element['#original_link']['mlid'];

//   if(is_array($element['#attributes']['class'])) {
// 	  foreach ($element['#attributes']['class'] as $idx => $item) {
// 	   //   if (strpos($item, 'mlid') !== FALSE)
// 	   //   {
// 			 // unset($element['#attributes']['class'][$idx]);
// 	   //   }
// 	  }
//   }
//   $sub_menu = '';
//   if ($element['#below']) {
//     $sub_menu = drupal_render($element['#below']);
//   }
//   $output = l($element['#title'], $element['#href'], $element['#localized_options']);
//   return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
// }

// function colibris_menu_contextual_links_alter(&$links, $router_item, $root_path) {
// 	if(isset($links['field_collection-edit'])) {
// 		$links = array_reverse($links);
// 	}
// }

// function colibris_form_search_api_page_search_form_search_alter(&$form, &$form_state, $form_id) {
// 	$form['keys_1']['#title']= t('keywords');
// 	$form['keys_1']['#attributes']['placeholder'] = t('keywords');
// }

function fabrique_form_alter(&$form, &$form_state, $form_id)
{
  switch ($form_id) {
      case 'views_exposed_form' :
        if ($form['#id'] == 'views-exposed-form-projets-page-2' or $form['#id'] == 'views-exposed-form-projets-page') {
          $form['keys']['#attributes']['placeholder'] = t('Rechercher');
          $form["#action"] = '/'.drupal_get_path_alias();
          //var_dump($form);
        }
        break;
      case 'user_login':
            drupal_set_title(t('Bienvenue sur la Fabrique des colibris'));
            $form['#prefix'] = '<p class="intro-user-login">
Pour déposer votre projet,<br>
nous vous invitons à vous connecter à votre compte utilisateur.</p>
<div class="white-box">';
            $form['name']['#title'] = t('Votre Email');
            $form['actions']['submit']['#prefix'] = '<a class="link-password" href="'.drupal_get_path_alias('/user/password').'">Mot de passe perdu ?</a>';
            $form['actions']['submit']['#value'] = t('Se connecter');
            $form['actions']['submit']['#attributes'] = array(
              'class' => array('btn-connexion')
            );
            $form['#suffix'] = '</div>';
         break;
//         case 'user_pass':
//         $form['#prefix'] = '<div class="grid-fit">
// <div class="box">
// <h1 class="page-name">nouveau mot de passe</h1>
// <p>
// Indiquez votre Email pour recevoir un courriel avec la procédure pour réinitialiser votre mot de passe.</p>
// </div>
// <div class="box">';
//         $form['actions']['submit']['#value'] = t('Renvoyer le mot de passe');
//         $form['actions']['submit']['#attributes'] = array(
//           'class' => array('btn-lostpassword')
//         );
//         $form['#suffix'] = '</div>
// <div class="box">
// <p>
// Si vous vous rappelez de vos identifiants :<br>
// <i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
//  <a href="'.drupal_get_path_alias('/user').$destination.'" class="fat-link"> Connectez vous à votre espace</a>
// </p>
// <p>
// Si vous n’avez pas de compte :<br>
// <i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
//  <a href="'.drupal_get_path_alias('/user/register').$destination.'" class="fat-link">Créer un nouveau compte</a>
// </p>
// </div>
// </div> <!-- end .grid-fit -->';
//             break;
      case 'user_register_form':
          drupal_set_title(t('Créer votre compte'));
          //$form['#prefix'] = '<div class="one-half white-box">';
          //$form['#suffix'] = '</div>';
          break;
//         case 'user_profile_form':
//             $form['#prefix'] = '<div class="grid-fit">
// <div class="box">
// <h1 class="page-name">mon compte</h1>
// <p>
// <i class="txt-green fa fa-check" aria-hidden="true"></i> Changez vos informations facilement<br><br>
// <i class="txt-green fa fa-check" aria-hidden="true"></i> Nous nous engageons à protéger vos données.
// </p>
// </div>
// <div class="box">';
//             $form['#suffix'] = '</div>
// <div class="box">
// <p>
// Si vous souhaitez juste voir vos formations suivies :<br>
// <i class="txt-yellow fa fa-long-arrow-right" aria-hidden="true"></i>
//  <a href="'.drupal_get_path_alias('/user/').'" class="fat-link">Revenir à mon tableau de bord</a>
// </p>
// </div>
// </div> <!-- end .grid-fit -->';
//             break;
    }
}
