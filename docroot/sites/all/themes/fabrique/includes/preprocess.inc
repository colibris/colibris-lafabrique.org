<?php
function fabrique_preprocess_block(&$variables)
{
    // Add class including block-my-subject
    if (!empty($variables['block']->subject)) {
        $variables['classes_array'][] = drupal_html_class('block-' . $variables['block']->subject);
    }
    if (!empty($variables['block']->delta)) {
        $variables['classes_array'][] = drupal_html_class('block-' . $variables['block']->delta);
    }
    if (!empty($variables['block']->css_class)) {
        $variables['classes_array'][] = drupal_html_class($variables['block']->css_class);
    }
}

function fabrique_preprocess_user_profile(&$variables)
{
  $account = $variables['elements']['#account'];
  //Add the user ID into the user profile as a variable
  $variables['user_id'] = $account->uid;
  // Helpful $user_profile variable for templates.
  foreach (element_children($variables['elements']) as $key) {
    $variables['user_profile'][$key] = $variables['elements'][$key];
  }

  // Preprocess fields.
  field_attach_preprocess('user', $account, $variables['elements'], $variables);

  $variables['user_profile']['privatemsg_send_new_message']['#title'] = 'Envoyer un message privé';
  $variables['user_profile']['privatemsg_send_new_message']['#options']['attributes']['class'] = 'button yellow';


  // if returning from the CAS server profile editing form (request parameter 'refresh' is 'profile'), logout then login in order to update the user account
  // verify also that the cas module is installed
  if (isset($_GET['refresh']) && $_GET['refresh'] == 'profile' && module_exists('cas') && function_exists('cas_login_check')) {
    user_logout_current_user();
    cas_login_check(true);
  }
}

/**
 * @file
 * Prepare variables for template files.
 */

/**
 * Implements template_preprocess_html().
 */
function fabrique_preprocess_html(&$vars)
{
  // Responsive Support
  drupal_add_html_head(array(
    '#type'         => 'html_tag',
    '#tag'          => 'meta',
    '#attributes'   => array(
      'name'      => 'viewport',
      'content'   => 'width=device-width, initial-scale=1'
    )
  ), 'viewport');

  // XUA Support
  drupal_add_html_head(array(
    '#type'         => 'html_tag',
    '#tag'          => 'meta',
    '#attributes'   => array(
      'content'   => 'IE=edge',
      'http-equiv'=> 'X-UA-Compatible'
    )
  ), 'xua');

    if ($vars['is_admin']) {
        $vars['classes_array'][] = 'admin';
    }

    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    $temp = explode('/', $path, 2);
    $section = array_shift($temp);
    $page_name = array_shift($temp);

    if (isset($page_name)) {
        $vars['classes_array'][] = drupal_html_id('page-'.$page_name);
    }

    $vars['classes_array'][] = drupal_html_id('section-'.$section);

    if (arg(0) == 'node') {
        if (arg(1) == 'add') {
            if ($section == 'node') {
                array_pop($vars['classes_array']); // Remove 'section-node'
            }
            $vars['classes_array'][] = 'section-node-add'; // Add 'section-node-add'
        } elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
            if ($section == 'node') {
                array_pop($vars['classes_array']); // Remove 'section-node'
            }
            $vars['classes_array'][] = 'section-node-'.arg(2); // Add 'section-node-edit' or 'section-node-delete'
        }
    }
}

function fabrique_preprocess_page(&$variables)
{
    // Only act when the first part of the url equals 'user', the second
    // part is a number and the third part is empty.
    $arg = arg();
    if ($arg[0] == 'user' && is_numeric($arg[1]) && empty($arg[2]) and $arg[1] == $variables['user']->uid) {
        // Do stuff to create the desired username and set the $title variable for the template.
        $variables['title'] = 'Mes projets';
    }
}

/**
 * Implements template_preprocess_node().
 */
function fabrique_preprocess_node(&$vars)
{
    if ($vars['node_url'] == '/deposer-un-projet') {
        // dpm($vars);
        $key = array_shift(array_keys($vars["page"]["content"]["system_main"]["nodes"]));
        $vars['content']["body"]["0"]["#markup"] = <<<EOF
<div class="lead">Pour déposer votre projet sur la Fabrique,<br>
voici <span class="text-orange">5 étapes</span> importantes à ne pas manquer !</div>

<div class="step white-bg">
<div class="header"><span class="text-orange number">01</span><span class="text-orange"><i class="fa fa-check"></i></span> Vérifiez que votre projet correspond aux <span class="text-orange">critères</span> de la plateforme</div>
<div class="desc">
<span class="text-orange">La Fabrique soutient tous les projets écologiques et solidaires qui sont :</span>
<img class="img-center criteres" alt="criteres" src="sites/all/themes/fabrique/images/criteres.png" />
</div>
<div class="footer">
A contrario, votre projet ne sera pas validé si c'est un événement ponctuel, une initiative privée ou si vous en êtes au stade de l'idée. </div>
</div>

<div class="step white-bg">
<div class="header"><span class="text-orange number">02</span><span class="text-orange"><i class="fa fa-user"></i></span> Créer votre compte sur la plateforme</div>
<div class="desc">
<span class="text-orange">Pour renseigner votre profil de porteur de projet, munissez-vous bien de :</span>
<ul>
<li>l'adresse email à laquelle vous souhaitez être contacté par les citoyens et les colibris</li>
<li>une photo de vous, voir qui est le porteur de projet, c'est plus sympa et plus humain!<br />
(Fichiers de type <strong>.png</strong>, <strong>.gif</strong>, <strong>.jpg</strong> ou <strong>.jpeg</strong>, et pesant moins de <strong>2 Mo</strong>.)</li>
<li>votre mini biographie. En quelques lignes, parlez-nous de vous!<br />
Et uniquement de vous, pas encore de votre projet, ça, c'est l'étape d'après.</li>
</ul>
</div>
<div class="footer">
Après avoir créé votre compte, un email de validation vous est envoyé. Suivre les indications de ce message pour obtenir un accès complet au site, et enfin vous lancer dans la présentation de votre projet.</div>
</div>

<div class="step white-bg">
<div class="header"><span class="text-orange number">03</span><span class="text-orange"><i class="fa fa-pencil"></i></span> Écrivez votre fiche projet</div>
<div class="desc">
<span class="text-orange">C’est une étape cruciale.<br />
C’est le moment de présenter via le formulaire votre projet et vos besoins de façon précise et inspirante.</span>
<ul>
<li>Préparez vous en ayant à portée de clics des photos pour illustrer votre projet (format paysage). Elles sont obligatoires.</li>
</ul>
</div>
<div class="footer">
Pour vous accompagner dans l’écriture, vous pouvez <a href="https://www.colibris-lafabrique.org/sites/all/docs/Guideduporteurdeprojet_laFabrique_V1.pdf" title="télécharger le guide du porteur de projet">télécharger le guide du porteur de projet</a> ou suivre à la lettre les textes d’aide dans le formulaire.
</div>
</div>

<div class="step white-bg">
<div class="header"><span class="text-orange number">04</span><span class="text-orange"><i class="fa fa-moon-o"></i></span> Patientez, rêvez, siestez en attendant la mise en ligne de votre projet</div>
<div class="desc">
<span class="text-orange">Une fois la fiche de votre projet enregistrée, elle n’est pas visible de suite sur le site.
</span>
<ul>
<li>une phase de modération permet à l’équipe de Colibris de vous soutenir dans la mise en ligne de votre projet et de valider sa cohérence par rapport à l’usage de la Fabrique (voir les critères ci-dessus).</li>
</ul>
</div>
<div class="footer">
Il faut compter en moyenne 1 semaine avant d’avoir de nos nouvelles!
</div>
</div>

<div class="step white-bg">
<div class="header"><span class="text-orange number">05</span><span class="text-orange"><i class="fa fa-glass"></i></span> Célébrez, arrosez, partagez la mise en ligne de votre projet</div>
<div class="desc">
<span class="text-orange">Youpi, votre projet est en ligne sur la Fabrique et nous en sommes ravis!
</span>
<ul>
<li>parlez autour de vous, sur votre site, par mail<br />
ou sur les réseaux sociaux via les boutons de partage disponibles sur la page de votre projet.</li>
</ul>
</div>
<div class="footer">Une fois votre projet en ligne, c'est vous qui avez la main pour mettre à jour votre présentation et vos demandes de soutien, sans nouvelle phase de modération.
</div>
</div>
<div class="call-to-action text-center">
<h4>C'EST PARTI</h4>
EOF;
if (user_is_logged_in()) {
    $vars['content']["body"]["0"]["#markup"] .= '<a href="/deposer" class="button yellow big"><i class="fa fa-plus"></i> JE DÉPOSE MON PROJET</a>';
} else {
    $vars['content']["body"]["0"]["#markup"] .= '<a href="/user/register?destination=deposer-un-projet" class="button yellow big"><i class="fa fa-user"></i> JE CRÉÉ MON COMPTE SUR LA FABRIQUE</a> ou <a href="https://monprofil.colibris-lemouvement.org/cas/login?service=https%3A%2F%2Fcolibris-lafabrique.org%2Fcas%3Fdestination%3Ddeposer-un-projet" class="button yellow big"><i class="fa fa-sign-in"></i> JE ME CONNECTE</a>
    <h4>POUR DÉPOSER MON PROJET</h4>';
}
$vars['content']["body"]["0"]["#markup"] .= '</div>';
    }
}

// /**
//  * Implements template_preprocess_block_wrapper().
//  */
// function colibris_preprocess_menu_block_wrapper(&$vars)
// {
//     $vars['classes_array'] = array('menu-block-'.$vars['id']);
// }

// /**
//  * Implements template_preprocess_block().
//  */
// function colibris_preprocess_block(&$vars, $hook)
// {
//     // Add a striping class.
//     $vars['classes_array'][] = 'block-'.$vars['zebra'];

//     $vars['title_attributes_array']['class'][] = 'block-title';

//     // Use nav element for menu blocks and provide a suggestion for all of them
//     $nav_blocks = array('navigation', 'main-menu', 'management', 'user-menu');
//     $nav_modules = array('superfish', 'nice_menus', 'menu_block');
//     if (in_array($vars['block']->delta, $nav_blocks) || in_array($vars['block']->module, $nav_modules)) {
//         $vars['tag'] = 'nav';
//         array_unshift($vars['theme_hook_suggestions'], 'block__menu');
//     } elseif (!empty($vars['block']->subject)) {
//         $vars['tag'] = 'section';
//     } else {
//         $vars['tag'] = 'div';
//     }

//   // In the header region visually hide block titles.
//     if ($vars['block']->region == 'header') {
//         if ($vars['id'] != 1) {
//             $vars['title_attributes_array']['class'][] = 'element-invisible';
//         }
//     }
// }

// /**
//  * Implements template_proprocess_search_block_form().
//  *
//  * Changes the search form to use the HTML5 "search" input attribute
//  */
// function colibris_preprocess_search_block_form(&$vars)
// {
//     $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
// }

// /**
//  * Implements template_proprocess_preprocess_menu_local_task().
//  *
//  * Override or insert variables into theme_menu_local_task().
//  */
// function colibris_preprocess_menu_local_task(&$vars)
// {
//     $link = &$vars['element']['#link'];

//     // If the link does not contain HTML already, check_plain() it now.
//     // After we set 'html'=TRUE the link will not be sanitized by l().
//     if (empty($link['localized_options']['html'])) {
//         $link['title'] = check_plain($link['title']);
//     }

//     $link['localized_options']['html'] = true;
//     $link['title'] = '<span class="tab">'.$link['title'].'</span>';
// }

// /**
//  * Generate doctype for templates.
//  */
// function _colibris_doctype()
// {
//     return (module_exists('rdf')) ? '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN"'."\n".'"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">' : '<!DOCTYPE html>'."\n";
// }

// /**
//  * Generate RDF object for templates.
//  *
//  * Uses RDFa attributes if the RDF module is enabled
//  * Lifted from Adaptivetheme for D7, full credit to Jeff Burnz
//  * ref: http://drupal.org/node/887600
//  *
//  * @param array $vars
//  */
// function _colibris_rdf($vars)
// {
//     $rdf = new stdClass();

//     if (module_exists('rdf')) {
//         $rdf->version = 'version="HTML+RDFa 1.1"';
//         $rdf->namespaces = $vars['rdf_namespaces'];
//         $rdf->profile = ' profile="'.$vars['grddl_profile'].'"';
//     } else {
//         $rdf->version = '';
//         $rdf->namespaces = '';
//         $rdf->profile = '';
//     }

//     return $rdf;
// }

// function colibris_html_head_alter(&$head_elements)
// {
//     foreach ($head_elements as $key => $element) {
//         if (isset($element['#attributes']['rel']) && ($element['#attributes']['rel'] == 'canonical' || $element['#attributes']['rel'] == 'shortlink') && $element['#attributes']['href'] != '/') {
//             unset($head_elements[$key]);
//         }
//     }
//     drupal_add_html_head_link(array(
//         'rel' => 'canonical',
//         'href' => '/',
//     ));
// }
