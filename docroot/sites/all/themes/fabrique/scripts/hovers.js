/*
 *           mouseover effects
 */

(function($) {
	$(document).ready(function(){
		$('a').each(function() {
		   var a = new RegExp('/' + window.location.host + '/');
		   if(!a.test(this.href)) {
		       $(this).attr('target','_blank');
		   }
		});
		$('.menu a').jrumble({
			x: 0,
			y: 1,
			rotation: .5
		});

		$('.menu a').hover(function(){
			$(this).trigger('startRumble');
		}, function(){
			$(this).trigger('stopRumble');
		});
	});
})(jQuery);
