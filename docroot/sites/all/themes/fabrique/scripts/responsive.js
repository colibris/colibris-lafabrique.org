// ;(function($) {
//   function topbar(context) {
//     if (context == 'mobile') {
//       if (!$('#archipel-colibris').hasClass('mobile-processed')) {
//         $('#archipel-colibris').addClass('mobile-processed');
//         $('#archipel-colibris span, .islands').hide();
//         $('#archipel-colibris .wrapper').append('<select id="islands"/>');
//         $('.islands a').each(function () {
//           $('#islands').append('<option value="' + $(this).attr('href') + '">' + $(this).html() + '</option>');
//         });
//         $('#islands').niceSelect();
//         $('#islands ~ .nice-select [data-value="https://www.colibris-lemouvement.org/"]')
//           .removeClass('selected').removeClass('focus');
//         $('#islands ~ .nice-select [data-value="https://www.colibris-universite.org/"]')
//           .addClass('selected').addClass('');

//         $('#islands').change(function () {
//           window.location = $(this).val();
//         });
//         $('.mobile-toggle').click(function () {
//           $('body').toggleClass('mobile-menu');
//         });
//       }
//     } else {
//       $('#archipel-colibris').removeClass('mobile-processed');
//       $('#archipel-colibris span, .islands').show();
//       $('#islands').remove();
//       $('#archipel-colibris .nice-select').remove();
//     }
//   }

//   function submenus(context){
//     if(context == 'mobile') { //mobile
//       $('.main-menu li').each(function(){
//         var classNames = $(this).attr('class').toString().split(' ');
//         var menuClass;
//         $.each(classNames, function (i, className) {
//           if(className.match("^menu")) {
//             menuClass = className;
//           }
//         });

//         $(this).append($('#' + menuClass, '.submenus'));

//       });

//     } else { //desktop
//       $('.submenus .wrapper').append($('.submenu'));

//       $('.main-menu li').each(function(){
//         var classNames = $(this).attr('class').toString().split(' ');
//         var menuClass;
//         $.each(classNames, function (i, className) {
//           if(className.match("^menu")) {
//             menuClass = className;
//           }
//         });
//       });

//     }
//   }

//   function moveToMobile() {
//     submenus('mobile');
//     topbar('mobile');
//   }
//   function moveToDesktop() {
//     submenus('desktop');
//     topbar('desktop');
//   }

// mediaCheck({
//     media: '(max-width: 768px)',
//     entry: function() {
//       moveToMobile();
//     },
//     exit: function() {
//       moveToDesktop();
//     },
// });
// }) (jQuery);
