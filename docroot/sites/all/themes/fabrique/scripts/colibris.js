(function($) {
  $(document).bind("leaflet.map", function(event, map, lMap) {
    // zoom et centrage sur le centre de la france pour la carte des projets
    if ($("body").hasClass("page-carte-des-projets")) {
      lMap.flyTo(new L.LatLng(46.93, 2.57), 6);
    }
  });
  $(document).ready(function() {
    /* on transforme les labels en placeholder */
    $(
      "#sendinblue-signup-subscribe-block-lettre-des-colibris-form :input, #user-register-form :input, #user-profile-form :input, #user-login :input, #user-pass :input"
    ).each(function(index, elem) {
      var eId = $(elem).attr("id");
      var label = null;
      if (
        eId &&
        (label = $(elem)
          .parents("form")
          .find(
            "label[for=" +
              eId +
              ']:not([for="edit-picture-upload"],[for="edit-picture-delete"])'
          )).length == 1
      ) {
        $(elem).attr("placeholder", $(label).text());
        $(label).hide();
      }
    });

    $('#views-exposed-form-besoins-page-1 #edit-keys').attr('placeholder', $('#views-exposed-form-besoins-page-1 #edit-keys').val()).attr('value', '');

    $(".views-exposed-form .form-select")
      .not(".niced")
      .addClass("niced")
      .niceSelect();
  });

  // // dirty hack to get contact info
  // Drupal.behaviors.ProjetsContact = {
  //   attach: function (context, settings) {
  //     var values = '';
  //     if(Drupal.settings.ProjetsContact.pnid.length) {
  //       values += Drupal.settings.ProjetsContact.pnid;
  //     }
  //     if(Drupal.settings.ProjetsContact.nnid.length) {
  //       values += '/' + Drupal.settings.ProjetsContact.nnid;
  //     }
  //     $('.webform-component--data input').val(values);
  //   }
  // };
})(jQuery);
