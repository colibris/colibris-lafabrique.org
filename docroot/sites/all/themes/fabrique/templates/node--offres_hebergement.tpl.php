<div class="hosting-container" <?php print $attributes; ?>>
    <div class="hosting-desc">
      <header>
        <div>
        <?php if (!empty($node->field_imageurl["und"][0]["safe_value"])) {
    echo '<img class="hosting-img" src="'.$node->field_imageurl["und"][0]["safe_value"].'" alt="image de l\'hébergement" />'."\n";
}
        ?>
      </div>
      <div>
        <p class="capacity">
          <strong>Capacité d'accueil :</strong><br>
          de <?php echo $node->field_mincapacity["und"][0]["value"]; ?>
          à <?php echo $node->field_maxcapacity["und"][0]["value"]; ?> personnes
        </p>
        <p class="price">
          <strong>Prix :</strong><br>
          <?php echo $node->field_price["und"][0]["value"]; ?>
        </p>
        <a class="button" href="<?php echo($node->field_bookingurl["und"][0]["url"]); ?>">Réserver</a>
      </div>
      </header>
      <?php print render($content['body']); ?>
    </div>
    <div class="hosting-aside">
      <?php echo '<h3>'.$node->field_projet["und"][0]['entity']->title.'</h3>';
      $view = field_view_field('node', $node->field_projet["und"][0]['entity'], 'field_proj_photos', array('settings' => array('image_style' => 'large'), 'label' => 'hidden'));
      $image = field_get_items('node', $node->field_projet["und"][0]['entity'], 'field_proj_photos');
      $output = field_view_value('node', $node->field_projet["und"][0]['entity'], 'field_proj_photos', $image[0], array(
        'type' => 'image',
        'label' => 'hidden',
        'settings' => array(
          'image_style' => 'large',
        ),
      ));
      print render($output);
      echo $node->field_projet["und"][0]['entity']->field_accroche['und'][0] ["safe_value"];
      echo $node->field_projet["und"][0]['entity']->field_proj_website['und'][0]["url"];
      ?>
    </div>
</div>
