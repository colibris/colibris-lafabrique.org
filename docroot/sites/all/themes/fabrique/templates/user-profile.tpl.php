<?php

// if CAS Drupal module is set, build the cas server url
$cas_host = (string)variable_get('cas_server', '');
if (isset($cas_host)) {
    $cas_port = (int) variable_get('cas_port', '443');
    $cas_host_url = 'https://' . $cas_host;
    if ($cas_port != 443) {
        $cas_host_url .= ':' . $cas_port;
    }
}

$currentuser = menu_get_object('user');
$current_path = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$edit_path = (!isset($cas_host_url) || empty($currentuser->field_cas_uid['und'][0]['value'])) ?
  "/" . drupal_get_path_alias('user/'.$currentuser->uid.'/edit') :
  $cas_host_url . "/user/" . $currentuser->field_cas_uid['und'][0]['value'] . "/edit?redirect=" . urlencode($current_path . "?refresh=profile");

$isMyProfile = (isset($user->uid) and ($user->uid === $currentuser->uid));
if ($isMyProfile) {
}

?>
<div class="profile"<?php print $attributes; ?>>
  <div class="group-two-third">
    <?php
    $query = new EntityFieldQuery();
    if ($isMyProfile) {
        $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'projet')
        ->propertyCondition('uid', $user->uid)
        ->execute();
    } else {
        $result = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'projet')
        ->propertyCondition('status', 1) // only show validated one
        ->propertyCondition('uid', $user->uid)
        ->execute();
    }
    $nodes = node_load_multiple(array_keys($result['node']));

    $projects = '';
    foreach ($nodes as $projet) {
        //krumo($projet);
        $projects .= '<li class="projet clearfix">';
        $style='projet_large';
        $path=$projet->field_proj_photos['und']['0']['uri'];
        $style_url = image_style_url($style, $path);
        $projects .= '<div class="img">
        <a href="'.url('node/'.$projet->nid).'">
        <img typeof="foaf:Image" src="'.file_create_url($style_url).'" alt="image '.htmlentities($projet->title).'" width="400" height="215">
        </a>
        </div>
        <div class="title"><a href="'.url('node/'.$projet->nid).'"><h3>'.$projet->title.'</h3></a></div>
        <div class="desc">'.render($projet->field_accroche).'</div>';
        if ($isMyProfile) {
            $projects .= '<div class="actions-buttons"><a class="button yellow modify" href="'.url('node/'.$projet->nid.'/edit').'">Modifier</a>  ';
            $projects .= '<a class="button black delete" href="'.url('node/'.$projet->nid.'/delete').'">Supprimer</a></div>';
        } else {
            $projects .= '<div class="more"><a href="'.url('node/'.$projet->nid).'">Découvrir</a></div>';
        }
        $projects .= '</li>';
    }
    if (empty($projects)) {
        if ($isMyProfile) {
            $projects = '<strong><em>Vous n\'avez pas encore de projet associé à votre compte.</em></strong><br><br>Vous pouvez en déposer en cliquant sur le bouton "Déposer un projet".';
        } else {
            $projects = '<strong><em>Pas encore de projet associé à ce profil.</em></strong><br>';
        }
    } else {
        $projects = '<div class="view-projets user-projets"><div class="item-list project-list"><ul>'."\n".$projects."\n".'</ul></div></div>';
    }
    echo $projects;
    ?>
  </div>
  <div class="group-one-third user-info">
    <div id="name" class="name-group">
      <?php if (!empty($currentuser->field_prenom['und'][0]['value'])) : ?>
      <div class="field field-name-field-prenom field-type-text field-label-hidden">
        <div class="field-items">
          <div class="field-item even"><?php echo $currentuser->field_prenom['und'][0]['value']; ?></div>
        </div>
      </div>
      <?php endif; ?>
      <?php if (!empty($currentuser->field_nom['und'][0]['value'])) : ?>
      <div class="field field-name-field-nom field-type-text field-label-hidden">
        <div class="field-items">
          <div class="field-item even"><?php echo $currentuser->field_nom['und'][0]['value']; ?></div>
        </div>
      </div>
      <?php endif; ?>
    </div>
    <?php if (!empty($currentuser->field_user_desc['und'][0]['value'])) : ?>
    <div class="field field-name-field-user-desc field-type-text-long field-label-hidden">
      <div class="field-items">
        <div class="field-item even"><?php echo $currentuser->field_user_desc['und'][0]['value']; ?></div>
      </div>
    </div>
    <?php endif; ?>
    <?php if (!empty($currentuser->field_url_photo['und']['0']['url'])) : ?>
    <div class="field field-name-field-url-photo field-type-link-field field-label-hidden">
      <div class="field-items">
        <div class="field-item even">
          <img class="profile-picture" src="<?php echo $currentuser->field_url_photo['und']['0']['url']; ?>" alt="profile picture" />
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php
      if ($isMyProfile) :
    ?>
    <a class="button black edit-user" href="<?php print $edit_path; ?>">
      <i class="fa fa-pencil" aria-hidden="true"></i>  Modifier mon profil
    </a>
    <a class="link-notifications button black" href="<?php echo $GLOBALS['base_url']; ?>/notifications">
      <i class="fa fa-envelope" aria-hidden="true"></i> Mes notifs nouveaux projets
    </a>
    <a href="/deposer-un-projet" class="button yellow ">
      <i class="fa fa-fw fa-plus" aria-hidden="true"></i>  Déposer un projet
    </a>
    <?php endif; ?>
  </div>
</div>
