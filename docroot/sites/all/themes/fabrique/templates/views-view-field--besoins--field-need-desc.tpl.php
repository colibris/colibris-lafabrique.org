<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php print $output; ?>

<?php
if ($row->node_type == 'need_money'): ?>
	<?php
    $row_node = node_load($row->nid);
    $pct = number_format($row_node->field_need_collect['und'][0]['value'] / $row_node->field_need_objectif['und'][0]['value'] * 100, 0, '.', ' ');
    $line_width = $pct > 100 ? 100 : $pct;
    ?>
	<div class="need-money">
		<div class="figures">
			<div class="data">
				<?php $out = field_view_field('node', $row_node, 'field_need_objectif');print render($out); ?>
				<?php $out = field_view_field('node', $row_node, 'field_need_collect');print render($out); ?>
			</div>
			<div class="graph">
				<div class="label"><?php print str_replace('.', ',', $pct); ?> %</div>
				<div class="total">
					<div class="amount" style="width: <?php print $line_width; ?>%"></div>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>
<?php
if ($row->node_type == 'offres_hebergement'): ?>
<?php
//dpm($row);
echo $row->field_body[0]['rendered']['#markup']; ?>
<?php endif; ?>