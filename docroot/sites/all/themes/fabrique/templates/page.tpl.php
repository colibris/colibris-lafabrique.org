<?php
 $user = user_load($GLOBALS['user']->uid);
?>
<header id="header" role="banner">
  <div id="archipel-colibris" class="archipel" role="banner"></div>
  <script>
    const url =  'https://colibris-lemouvement.org/archipel-markup?domain=colibris-lafabrique.org'
    var getJSON = function(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function() {
          var status = xhr.status;
          if (status === 200) {
            callback(null, xhr.response);
          } else {
            callback(status, xhr.response);
          }
        };
        xhr.send();
    };

    getJSON(url,
    function(err, data) {
      if (err !== null) {
        console.log('Something went wrong: ' + err);
      } else {
        document.getElementById('archipel-colibris').innerHTML = data.markup;
        var styleElement = document.createElement("style");
        styleElement.innerHTML = data.style;
        document.getElementById('archipel-colibris').appendChild(styleElement);
      }
    });
  </script>

!--
Bannière
-->


<center><a href="https://www.colibris-lemouvement.org/mooc-revolutions-locales/" style="display:block;">
<?php
// Tableau contenant les noms des images
$images = array("banner_mooc_revolutions_locales.jpg");

// Choix aléatoire d'une image
$image_choisie = $images[array_rand($images,1)];

// Affichage de l'image dans la page HTML
echo '<img class="img-responsive" src="https://www.colibris-lemouvement.org/sites/all/themes/fabrique/images/' . $image_choisie . '"  alt="Découvrez le MOOC (R)évolutions Locales pour s\'engager collectivement sur son territoire">';
?>
</a>
</center>
<!--
Fin Bannière
-->

 
  <nav class="navigation">
    <div class="wrapper clearfix">
      <a class="logo" href="<?php print $front_page; ?>" title="page d'accueil">
        <img src="<?php echo $GLOBALS['base_url']; ?>/sites/all/themes/fabrique/images/Logo_Colibris_RVB.svg" alt="Logo Mouvement Colibris" />
      </a>

      <div class="user-menu">
      <?php if (user_is_logged_in()) : ?>
        <span class="username margin-right"><?php print $user->field_prenom['und'][0]['value'] . ' ' . $user->field_nom['und'][0]['value'] ?><p></span>
        <a class="link-notifications primary-link margin-right" href="<?php echo $GLOBALS['base_url']; ?>/notifications">
          <i class="fa fa-envelope" aria-hidden="true"></i> Gérer mes notifications
        </a>
        <a class="primary-link margin-right" href="<?php echo $GLOBALS['base_url']; ?>/user/<?php echo $GLOBALS['user']->uid; ?>">
          <i class="fa fa-lightbulb-o" aria-hidden="true"></i> Mes projets
        </a>
        <a class="primary-link logout" href="<?php echo $GLOBALS['base_url']; ?>/user/logout">
          <i class="fa fa-sign-out" aria-hidden="true"></i> Déconnexion
        </a>
      <?php else : ?>
        <a class="link-notifications primary-link margin-right" href="<?php echo $GLOBALS['base_url']; ?>/notifications">
          <i class="fa fa-envelope" aria-hidden="true"></i> Recevoir les nouveaux projets par email
        </a>
        <a class="login margin-right" href="<?php echo $GLOBALS['base_url']; ?>/user">
          <i class="fa fa-sign-in" aria-hidden="true"></i> Connexion
        </a>
        <!--
        <a class="button yellow" href="<?php echo $GLOBALS['base_url']; ?>/user/register">
          <i class="fa fa-pencil" aria-hidden="true"></i> S'inscrire
        </a>
        -->
      <?php endif; ?>
      <br>
      <!--<a href="https://www.jedonneenligne.org/colibris/DONS/" class="button black margin-right"><i class="fa fa-fw fa-heart" aria-hidden="true"></i> Soutenir Colibris</a>-->
      <a href="<?php echo $GLOBALS['base_url']; ?>/deposer-un-projet" class="button yellow "><i class="fa fa-fw fa-plus" aria-hidden="true"></i> Déposer un projet</a>
      </div>
    </div>
  </nav>
  <?php
  if (!empty($messages)) {
      echo '<div class="wrapper">'.$messages.'</div>';
  }
  print render($page['header']);
  ?>
</header>
<div id="page">
  <div id="main">
    <div class="wrapper">
        <?php if (!drupal_is_front_page()) : ?>
          <div id="title-page">
              <?php print render($title_prefix); ?>
              <?php if ($title) : ?>
                <h1><?php print $title ?></h1>
              <?php endif; ?>
              <?php print render($title_suffix); ?>
          </div>
        <?php endif; ?>
        <?php if ($page['content_sidebar']) : ?>
            <div class="flex-2cols">
        <?php endif; ?>
        <?php echo render($page['content']); ?>
        <?php if ($page['content_sidebar']) : ?>
              <div class="sidebar">
                  <?php echo render($page['content_sidebar']);?>
              </div>
            </div>
        <?php endif; ?>


        <?php if ($tabs) : ?>
          <div id="tab-links">
            <?php echo render($tabs); ?>
          </div>
        <?php endif; ?>
    </div>
  </div>

    <?php if ($page['content_bottom']) : ?>
    <footer id="content_bottom" role="contentinfo">
      <div class="wrapper">
        <?php echo render($page['content_bottom']); ?>
      </div>
    </footer>
    <?php endif; ?>

    <?php if ($page['footer']) : ?>
    <footer id="footer" role="contentinfo">
      <div class="wrapper">
        <?php echo render($page['footer']); ?>
      </div>
    </footer>
    <?php endif; ?>
</div>
