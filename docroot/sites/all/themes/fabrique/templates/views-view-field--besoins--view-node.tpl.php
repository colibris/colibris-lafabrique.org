<?php
/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

if ($row->node_type == 'need_money') {
    $title = "Je soutiens";
    if (isset($row->_field_data['nid']['entity']->field_need_url['und'])) {
        $link = $row->_field_data['nid']['entity']->field_need_url['und'][0]['url'];
    } else {
        $link = '#';
    }
} elseif ($row->node_type == 'need_people') {
    $title = 'Ça m\'intéresse';
    $link = '/ctc/'.$row->node_field_data_field_projet_nid.'/'.$row->nid;
} elseif ($row->node_type == 'need_stuff') {
    $title = 'J\'ai';
    $link = '/ctc/'.$row->node_field_data_field_projet_nid.'/'.$row->nid;
} elseif ($row->node_type == 'offres_hebergement') {
    $title = 'En savoir plus';
    $link = drupal_get_path_alias('node/'.$row->nid);
}
?>
<a class="btn-action btn-<?php echo $row->node_type; ?>" href="<?php print $link; ?>"><?php print $title; ?></a>
