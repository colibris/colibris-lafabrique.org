<?php
if(isset($node->field_need_objectif['und']) && $node->field_need_objectif['und'][0]['value']) {
	$pct = number_format($node->field_need_collect['und'][0]['value'] / $node->field_need_objectif['und'][0]['value'] * 100, 0, '.', ' ');
} else {
	$pct = 0;
}
$line_width = $pct > 100 ? 100 : $pct;


?>
<div class="need need-money">
	<div class="row">
		<div class="type">
			Recherche <span>de financement</span>		
		</div>
		<div class="desc">
			<?php print nl2br($node->field_need_desc['und'][0]['value']); ?>
		</div>
		<div class="figures">
			<div class="data">
				<?php print render($content['field_need_objectif']); ?>
				<?php print render($content['field_need_collect']); ?>
			</div>
			<div class="graph">
				<div class="label"><?php print str_replace('.',',',$pct); ?> %</div>
				<div class="total">
					<div class="amount" style="width: <?php print $line_width; ?>%"></div>
				</div>
			</div>
		</div>
		<?php if(isset($node->field_need_url['und'])): ?>
			<div class="action">
				<?php print l('En savoir+',$node->field_need_url['und'][0]['url'],array('attributes' => array('target'=>'_blank', 'class'=>'button yellow btn-block'))); ?>
			</div>
		<?php endif; ?>
	</div>
</div>