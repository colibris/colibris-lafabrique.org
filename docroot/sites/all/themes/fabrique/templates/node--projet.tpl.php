<?php

/**
* @file
* Default theme implementation to display a node.
*/
$is_admin = false;
$contexts = context_active_contexts();
if (array_key_exists('admin', $contexts)) {
    $is_admin = true;
}

if (!$is_admin) {
    unset($content['group_mod']);
}

$author = user_load($node->uid);
?>
<div class="title-wrapper">
	<h1 class="title" id="page-title">
		<?php print $title;?>
		<span class="name"> par <?php print $name; ?></span>
	</h1>
	<?php print render($title_prefix); ?>
</div>
<div class="project-container" <?php print $attributes; ?>>
	<div class="projet-main">
		<div class="projet-desc">
			<?php
            hide($content['comments']);
            hide($content['links']);
            print render($content['field_proj_photos']);
            ?>

			<h2>Présentation détaillée du projet</h2>
			<?php print render($content['field_proj_desc']); ?>
			<?php print render($content['field_proj_website']); ?>
		</div>

		<div class="projet-needs" id="besoins">

			<?php if (isset($content['field_proj_money'])): ?>
				<div class="money needs">
					<h2>Financement</h2>
					<?php print render($content['field_proj_money']); ?>
				</div>
			<?php endif; ?>

			<?php if (isset($content['field_proj_people'])): ?>
				<div class="people needs">
					<h2>Ressources humaines</h2>
					<?php print render($content['field_proj_people']); ?>
				</div>
			<?php endif; ?>

			<?php if (isset($content['field_proj_stuff'])): ?>
				<div class="stuff needs">
					<h2>Matériel</h2>
					<?php print render($content['field_proj_stuff']); ?>
				</div>
			<?php endif; ?>

      <?php if (false): ?>
				<div class="offers needs">
					<h2>Offre d'hébergement</h2>
					<div class="field field-name-field-offers field-type-entityreference field-label-hidden">
            <div class="field-items">
              <div class="field-item even">
                <div class="need offer">
                  <div class="row">
                    <div class="type">
                      Je propose <span>youpi</span>
                    </div>
                    <div class="desc">description</div>
                    <div class="action">
                      <a href="https://www.helloasso.com/associations/l-echervelee/collectes/sauvez-les-echerveles" target="_blank" class="button pink btn-block">En savoir+</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
				</div>
			<?php endif; ?>

			<?php global $account; if (node_access("update", $node, $account) === true): ?>
				<a class="form-submit" href="/node/<?php print $node->nid; ?>/edit" class="btn">Modifier ce projet</a>
			<?php endif; ?>
		</div>
	</div>

	<div class="projet-side">
		<div class="block-share">
			<h3>J'aime<br/>Je partage</h3>
			<div class="social">
			<?php print render($content['social-bottom']); ?>
			</div>
		</div>

		<div class="block-author">
			<?php
            if (!empty($author->field_url_photo['und'][0]['url'])) {
                print '<img typeof="foaf:Image" src="'.$author->field_url_photo['und'][0]['url'].'" alt="Profile picture" class="profile-picture">';
            };
            ?>
			<h3><?php print $author->field_prenom['und'][0]['value'] . ' ' . $author->field_nom['und'][0]['value'] ?></h3>
			<?php print render($content['group_user_desc']['field_user_desc']); ?>
			<div class="contact">
        <a href="/ctc/<?php print $node->nid;?>" class="button btn-block yellow">
          <i class="fa fa-envelope"></i> Contacter <?php print $author->field_prenom['und'][0]['value']; ?>
        </a>
      </div>
		</div>

		<div class="block-map">
			<h3><i class="fa fa-map-marker"></i> Localisation</h3>
			<?php
            print render($content['field_geodata']);
            ?>
		</div>
	</div>
</div>
