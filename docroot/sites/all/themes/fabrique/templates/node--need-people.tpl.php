<div class="need need-people">
	<div class="row">
		<div class="type">
			Je recherche <span><?php print $node->title; ?></span>
		</div>
		<div class="desc">
			<?php print nl2br($node->field_need_desc['und'][0]['value']); ?>
		</div>
		<div class="action">
			<a href="/ctc/<?php print $node->field_projet['und'][0]['target_id']; ?>/<?php print $node->nid; ?>" class="button green btn-block">Ça m'intéresse</a>
		</div>
	</div>
</div>