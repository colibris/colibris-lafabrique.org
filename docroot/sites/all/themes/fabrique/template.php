<?php
$theme_path = 'sites/all/themes/fabrique';

require_once $theme_path . '/includes/assets.inc';
require_once $theme_path . '/includes/preprocess.inc';
require_once $theme_path . '/includes/theme.inc';
